﻿// -----------------------------------------------------------------------
// <copyright file="JobStatus.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using Delta.GEDCS.Client.Interfaces;

namespace Delta.GEDCS.Client.Model
{
    public class GEDCSApiResponse : IGEDCSApiResponse, INotifyPropertyChanged
    {
        #region IGEDCSApiResponse Members

        public string JobID { get; set; }
        public string JobURL { get; set; }
        public string JobStatus { get; set; }
        public string MessageCode { get; set; }
        public string MessageDescription { get; set; }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}