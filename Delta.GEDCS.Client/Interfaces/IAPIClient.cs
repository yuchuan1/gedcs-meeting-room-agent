using Delta.GEDCS.Client.Model;

namespace Delta.GEDCS.Client.Interfaces
{
    public interface IAPIClient
    {
        GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, string xmlFileLocation, string xsltFileLocation);

        GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, string xmlFileLocation, string xsltFileLocation,
                                              string title);

        GEDCSApiResponse SendPNGToEtag(string etagName, string pngFilePath);
        GEDCSApiResponse SendPNGToEtag(string etagName, string pngFilePath, string title);
        GEDCSApiResponse GetJobResult(string jobId);
        string GetJobResultResponseToString();
        string GetDeviceSize(string eTagId);
        string GetAdminViewXml();
        string GetTopologyXML();
    }
}