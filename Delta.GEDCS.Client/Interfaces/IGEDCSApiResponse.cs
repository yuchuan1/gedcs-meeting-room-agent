namespace Delta.GEDCS.Client.Interfaces
{
    public interface IGEDCSApiResponse
    {
        string JobID { get; set; }
        string JobURL { get; set; }
        string JobStatus { get; set; }
        string MessageCode { get; set; }
        string MessageDescription { get; set; }
    }
}