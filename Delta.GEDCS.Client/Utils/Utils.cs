﻿// -----------------------------------------------------------------------
// <copyright file="Utils.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;

namespace Delta.GEDCS.Client
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Utils
    {
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            if (!String.IsNullOrEmpty(filePath))
            {
                var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                try
                {
                    var length = (int) fileStream.Length; // get file length
                    buffer = new byte[length]; // create buffer
                    int count; // actual number of bytes read
                    int sum = 0; // total number of bytes read

                    // read until Read method returns 0 (end of the stream has been reached)
                    while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                        sum += count; // sum is a buffer offset for next reading
                }
                finally
                {
                    fileStream.Close();
                }
                return buffer;
            }
            return null;
        }
    }
}