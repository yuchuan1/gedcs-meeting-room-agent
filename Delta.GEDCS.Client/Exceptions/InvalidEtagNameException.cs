﻿// -----------------------------------------------------------------------
// <copyright file="DeviceNotFoundException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Delta.GEDCS.Client.Exceptions
{
    public class InvalidEtagNameException : Exception
    {
        public InvalidEtagNameException()
        {
        }

        public InvalidEtagNameException(string message) : base(message)
        {
        }

        public InvalidEtagNameException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}