﻿// -----------------------------------------------------------------------
// <copyright file="InvalidEtagSize.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Delta.GEDCS.Client.Exceptions
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class InvalidEtagSize : Exception
    {
        public InvalidEtagSize()
        {
        }

        public InvalidEtagSize(string message) : base(message)
        {
        }

        public InvalidEtagSize(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}