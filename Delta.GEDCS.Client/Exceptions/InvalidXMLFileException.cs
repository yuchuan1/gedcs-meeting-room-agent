﻿using System;

namespace Delta.GEDCS.Client.Exceptions
{
    public class InvalidXMLFileException : Exception
    {
        public InvalidXMLFileException()
        {
        }

        public InvalidXMLFileException(string message) : base(message)
        {
        }

        public InvalidXMLFileException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}