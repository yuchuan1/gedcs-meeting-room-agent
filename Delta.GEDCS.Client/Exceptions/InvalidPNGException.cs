﻿using System;

namespace Delta.GEDCS.Client.Exceptions
{
    public class InvalidPNGException : Exception
    {
        public InvalidPNGException()
        {
        }

        public InvalidPNGException(string message) : base(message)
        {
        }

        public InvalidPNGException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}