﻿using System;

namespace Delta.GEDCS.Client.Exceptions
{
    public class InvalidXSLTFileException : Exception
    {
        public InvalidXSLTFileException()
        {
        }

        public InvalidXSLTFileException(string message) : base(message)
        {
        }

        public InvalidXSLTFileException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}