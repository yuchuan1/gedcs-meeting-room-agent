﻿// -----------------------------------------------------------------------
// <copyright file="MeetingIDNotFoundException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Delta.GEDCS.Client.Exceptions
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MeetingIDNotFoundException : Exception
    {
        public MeetingIDNotFoundException()
        {
        }

        public MeetingIDNotFoundException(string message) : base(message)
        {
        }

        public MeetingIDNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}