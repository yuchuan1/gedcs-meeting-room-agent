﻿// -----------------------------------------------------------------------
// <copyright file="InvalidJobIDException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Delta.GEDCS.Client.Exceptions
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class InvalidJobIDException : Exception
    {
        public InvalidJobIDException()
        {
        }

        public InvalidJobIDException(string message) : base(message)
        {
        }

        public InvalidJobIDException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}