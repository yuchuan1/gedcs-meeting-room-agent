﻿// -----------------------------------------------------------------------
// <copyright file="APIClient.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Net;
using System.Web;
using Delta.GEDCS.Client.Interfaces;
using Delta.GEDCS.Client.Model;
using NLog;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Delta.GEDCS.Client.APIs
{
    //public sealed partial class APIClient
    public sealed partial class APIClient : IAPIClient
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        //private static volatile APIClient instance;
        //private static object syncRoot = new Object();


        //private APIClient() { }

        //public static APIClient Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            lock (syncRoot)
        //            {
        //                if (instance == null)
        //                    instance = new APIClient();
        //            }
        //        }

        //        return instance;
        //    }
        //}

        private GEDCSApiResponse ParseResult(string jobResult)
        {
            var result = new GEDCSApiResponse();
            _logger.Debug("Parsing JobResult: " + jobResult);
            JObject jObject;
            try
            {
                jObject = JObject.Parse(jobResult);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw;
            }

            if (jObject != null)
            {
                if (jObject["messageCode"] != null)
                {
                    result.MessageCode = jObject["messageCode"].ToString();
                    _logger.Debug("messageCode=" + result.MessageCode);
                }
                if (jObject["messageDescription"] != null)
                {
                    result.MessageDescription = jObject["messageDescription"].ToString();
                    _logger.Debug("messageDescription=" + result.MessageDescription);
                }
                if (jObject["jobID"] != null)
                {
                    result.JobID = jObject["jobID"].ToString();
                    _logger.Debug("jobID=" + result.JobID);
                }
                if (jObject["jobStatus"] != null)
                {
                    result.JobStatus = jObject["jobStatus"].ToString();
                    _logger.Debug("jobStatus=" + result.JobStatus);
                }
                if (jObject["jobURL"] != null)
                {
                    result.JobURL = jObject["jobURL"].ToString();
                    _logger.Debug("jobURL=" + result.JobURL);
                }
            }
            return result;
        }

        private string RestResponseHandler(RestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    _logger.Debug("HTTP status code:" + response.StatusCode);
                    _logger.Debug("ResponseUri: " + response.ResponseUri);
                    _logger.Debug("ResponseStatus: " + response.ResponseStatus);
                    _logger.Debug("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.NotFound:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: "  + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.Forbidden:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: "  + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.InternalServerError:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: "  + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.BadGateway:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: "  + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                case HttpStatusCode.BadRequest:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: "  + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
                default:
                    _logger.Error("HTTP status code:" + response.StatusCode);
                    _logger.Error("ResponseUri: "  + response.ResponseUri);
                    _logger.Error("ResponseStatus: " + response.ResponseStatus);
                    _logger.Error("Content: \n" + response.Content);
                    return response.Content;
            }
        }
    }
}