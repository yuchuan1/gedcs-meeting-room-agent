﻿// -----------------------------------------------------------------------
// <copyright file="GetJobResult.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.Text;
using Delta.GEDCS.Client.Interfaces;
using Delta.GEDCS.Client.Model;
using RestSharp;

namespace Delta.GEDCS.Client.APIs
{
    //public sealed partial class APIClient
    public partial class APIClient : IAPIClient
    {
        private string _jobId;

        #region IAPIClient Members

        public GEDCSApiResponse GetJobResult(string jobId)
        {
            _jobId = jobId;
            return ParseResult(GetJobResultFromServer(jobId));
        }

        public string GetJobResultResponseToString()
        {
            GEDCSApiResponse gedcsApiResponse = GetJobResult(_jobId);
            var resultSB = new StringBuilder();
            resultSB.AppendLine("Job ID: " + gedcsApiResponse.JobID);
            resultSB.AppendLine("messageCode: " + gedcsApiResponse.MessageCode);
            resultSB.AppendLine("messageDescription: " + gedcsApiResponse.MessageDescription);
            resultSB.AppendLine("jobStatus: " + gedcsApiResponse.JobStatus);
            resultSB.AppendLine("jobURL: " + gedcsApiResponse.JobURL);

            return resultSB.ToString();
        }

        #endregion

        private string GetJobResultFromServer(string jobId)
        {
            var client = new RestClient
                             {
                                 BaseUrl = ConfigurationManager.AppSettings["GDECSBaseUrl"]
                                           + ConfigurationManager.AppSettings["JobStatusUrl"]
                             };

            _logger.Debug("Requesting job result: " + client.BaseUrl);

            var request = new RestRequest();
            request.AddParameter("jobID", jobId);
            request.Method = Method.GET;
            _logger.Debug("Requesting " + client.BaseUrl + " with parameter jobID=" + jobId);
            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }
    }
}