﻿// -----------------------------------------------------------------------
// <copyright file="GEDCSAdminView.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using Delta.GEDCS.Client.Interfaces;
using RestSharp;

namespace Delta.GEDCS.Client.APIs
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public partial class APIClient : IAPIClient
    {
        #region IAPIClient Members

        public string GetTopologyXML()
        {
            var client = new RestClient
                             {
                                 BaseUrl = ConfigurationManager.AppSettings["GDECSBaseUrl"]
                                           + ConfigurationManager.AppSettings["ReadAdminViewUrl"]
                             };

            _logger.Debug("Requesting admin view: " + client.BaseUrl);

            var request = new RestRequest {Method = Method.GET};
            _logger.Debug("Requesting " + client.BaseUrl);

            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }

        #endregion
    }
}