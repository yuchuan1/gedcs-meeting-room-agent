﻿// -----------------------------------------------------------------------
// <copyright file="GetDeviceSize.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Interfaces;
using RestSharp;

namespace Delta.GEDCS.Client.APIs
{
    //public sealed partial class APIClient
    public partial class APIClient : IAPIClient
    {
        #region IAPIClient Members

        public string GetDeviceSize(string eTagId)
        {
            if (String.IsNullOrEmpty(eTagId))
                throw new InvalidEtagNameException("eTagId is empty");

            XDocument xDoc = XDocument.Parse(GetAdminViewXml());
            string eTagDeviceSize = (from etag in xDoc.Descendants("etag")
                                     where etag.Attribute("name").Value.Equals(eTagId)
                                     select etag.Descendants("panel_size").SingleOrDefault().Value).SingleOrDefault();

            if (String.IsNullOrEmpty(eTagDeviceSize) || String.IsNullOrEmpty(eTagId))
                throw new InvalidEtagNameException(eTagId + " not found!");

            return eTagDeviceSize;
        }

        public string GetAdminViewXml()
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["GDECSReadAdminViewUrl"]
                             };

            _logger.Debug("Requesting admin view: " + client.BaseUrl);
            var request = new RestRequest();
            var response = (RestResponse) client.Execute(request);

            return RestResponseHandler(response);
        }

        #endregion
    }
}