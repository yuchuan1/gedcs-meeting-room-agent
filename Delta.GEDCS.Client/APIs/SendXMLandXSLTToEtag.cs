﻿// -----------------------------------------------------------------------
// <copyright file="SendXMLandXSLTToEtag.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.Text;
using Delta.GEDCS.Client.Interfaces;
using Delta.GEDCS.Client.Model;
using RestSharp;

namespace Delta.GEDCS.Client.APIs
{
    //public sealed partial class APIClient
    public partial class APIClient : IAPIClient
    {
        #region IAPIClient Members

        public GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, string xmlFileLocation, string xsltFileLocation)
        {
            return ParseResult(SendXMLandXSLTToEtagToServer(etagName, xmlFileLocation, xsltFileLocation));
        }

        public GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, string xmlFileLocation, string xsltFileLocation,
                                                     string title)
        {
            return ParseResult(SendXMLandXSLTToEtagToServer(etagName, xmlFileLocation, xsltFileLocation, title));
        }

        #endregion

        public GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, byte[] xmlByteArray, string xsltFileLocation)
        {
            return ParseResult(SendXMLandXSLTToEtagToServer(etagName, xmlByteArray, xsltFileLocation));
        }

        public GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, byte[] xmlByteArray, string xsltFileLocation,
                                                     string title)
        {
            return ParseResult(SendXMLandXSLTToEtagToServer(etagName, xmlByteArray, xsltFileLocation, title));
        }


        private string SendXMLandXSLTToEtagToServer(string etagName, string xmlFileLocation, string xsltFileLocation)
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["SendXMLandXSLTtoeTagUrl"]
                             };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("etagName", etagName);
            byte[] xmlFile = Utils.ReadFile(xmlFileLocation);
            byte[] xsltFile = Utils.ReadFile(xsltFileLocation);
            request.AddFile("xml", xmlFile, "xmlFile.xml");
            request.AddFile("xslt", xsltFile, "xslt.xsl");
            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + " xml=" +
                          xmlFileLocation + " xslt=" + xsltFileLocation);
            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }

        private string SendXMLandXSLTToEtagToServer(string etagName, string xmlFileLocation, string xsltFileLocation,
                                                    string title)
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["SendXMLandXSLTtoeTagUrl"]
                             };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("etagName", etagName);
            request.AddParameter("title", title);
            byte[] xmlFile = Utils.ReadFile(xmlFileLocation);
            byte[] xsltFile = Utils.ReadFile(xsltFileLocation);
            request.AddFile("xml", xmlFile, "xmlFile.xml");
            request.AddFile("xslt", xsltFile, "xslt.xsl");
            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + " xml=" +
                          xmlFileLocation + " xslt=" + xsltFileLocation + " title=" + title);
            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }

        private string SendXMLandXSLTToEtagToServer(string etagName, byte[] xmlByteArray, string xsltFileLocation)
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["SendXMLandXSLTtoeTagUrl"]
                             };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("etagName", etagName);
            request.AddFile("xml", xmlByteArray, "MeetingInfo.xml");
            byte[] xsltFile = Utils.ReadFile(xsltFileLocation);
            request.AddFile("xslt", xsltFile, "etag.xsl");
            
            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + " xml=" +
                          "MeetingInfo.xml" + " xslt=" + xsltFileLocation);

            _logger.Debug("xslt content: \n" + Encoding.UTF8.GetString(xsltFile));

            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }


        private string SendXMLandXSLTToEtagToServer(string etagName, byte[] xmlByteArray, string xsltFileLocation,
                                                    string title)
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["SendXMLandXSLTtoeTagUrl"]
                             };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("etagName", etagName);
            request.AddParameter("title", title);
            request.AddFile("xml", xmlByteArray, "MeetingInfo.xml");
            byte[] xsltFile = Utils.ReadFile(xsltFileLocation);
            request.AddFile("xslt", xsltFile, "etag.xsl");

            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + " xml=" +
                          "MeetingInfo.xml" + " xslt=" + xsltFileLocation);

            _logger.Debug("xslt content: \n" + Encoding.UTF8.GetString(xsltFile));
            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }
    }
}