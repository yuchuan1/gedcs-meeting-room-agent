﻿// -----------------------------------------------------------------------
// <copyright file="SendPNGToEtag.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Configuration;
using System.IO;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Interfaces;
using Delta.GEDCS.Client.Model;
using RestSharp;

namespace Delta.GEDCS.Client.APIs
{
    //public sealed partial class APIClient
    public partial class APIClient : IAPIClient
    {
        #region IAPIClient Members

        public GEDCSApiResponse SendPNGToEtag(string etagName, string pngFilePath)
        {
            if (File.Exists(pngFilePath))
                return ParseResult(SendPNGToEtagToServer(etagName, pngFilePath));
            else
            {
                throw new InvalidPNGException(pngFilePath + " not found!");
            }
        }

        public GEDCSApiResponse SendPNGToEtag(string etagName, byte[] pngFile)
        {
            return ParseResult(SendPNGToEtagToServer(etagName, pngFile));
          
        }

        public GEDCSApiResponse SendPNGToEtag(string etagName, string pngFilePath, string title)
        {
            return ParseResult(SendPNGToEtagToServer(etagName, pngFilePath, title));
        }

        #endregion

        private string SendPNGToEtagToServer(string etagName, string pngFilePath)
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["SendPNGToEtagUrl"]
                             };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("etagName", etagName);
            request.AddParameter("title", "123");
            byte[] raw = Utils.ReadFile(pngFilePath);
            request.AddFile("raw", raw, "raw.png");
            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + "raw=" + pngFilePath);
            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }

        private string SendPNGToEtagToServer(string etagName, byte[] pngFile)
        {
            var client = new RestClient
            {
                BaseUrl =
                    ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                    ConfigurationManager.AppSettings["SendPNGToEtagUrl"]
            };

            var request = new RestRequest { Method = Method.POST };
            request.AddParameter("etagName", etagName);
            request.AddFile("xml", pngFile, "raw.png");

            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + "raw=raw.png");
            var response = (RestResponse)client.Execute(request);
            return RestResponseHandler(response);
        }

        private string SendPNGToEtagToServer(string etagName, string pngFilePath, string title)
        {
            var client = new RestClient
                             {
                                 BaseUrl =
                                     ConfigurationManager.AppSettings["GDECSBaseUrl"] +
                                     ConfigurationManager.AppSettings["SendPNGToEtagUrl"]
                             };

            var request = new RestRequest {Method = Method.POST};
            request.AddParameter("etagName", etagName);
            request.AddParameter("title", title);
            byte[] raw = Utils.ReadFile(pngFilePath);

            if (raw != null)
                request.AddFile("raw", raw, "raw.png");

            _logger.Debug("Requesting " + client.BaseUrl + " with parameter etagName=" + etagName + " raw=" +
                          pngFilePath + " title=" + title);
            var response = (RestResponse) client.Execute(request);
            return RestResponseHandler(response);
        }
    }
}