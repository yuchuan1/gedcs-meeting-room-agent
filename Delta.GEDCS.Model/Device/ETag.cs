﻿// -----------------------------------------------------------------------
// <copyright file="ETag.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Delta.GEDCS.Model.Device
{
    /// <summary>
    /// This class models an etag
    /// </summary>
    public class ETag : IETag
    {
        #region EtagSize enum

        public enum EtagSize
        {
            S4,
            S7
        }

        #endregion

        #region IETag Members

        public string Name { get; set; }
        public EtagSize Size { get; set; }

        #endregion
    }
}