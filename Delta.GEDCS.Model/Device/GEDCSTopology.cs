﻿// -----------------------------------------------------------------------
// <copyright file="GEDCSTopology.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Model.Interfaces;
using NLog;

namespace Delta.GEDCS.Model.Device
{
    public class GEDCSTopology : IGEDCSTopology
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public GEDCSTopology()
        {
            BaseStations = new List<BaseStation>();
        }

        public GEDCSTopology(string gedcsAdminViewXML)
        {
            GEDCSTopology topology = ParseGEDCSTopology(gedcsAdminViewXML);
            bs_count = topology.bs_count;
            gedcs_version = topology.gedcs_version;

            BaseStations = new List<BaseStation>();
            BaseStations = topology.BaseStations;
        }

        #region IGEDCSTopology Members

        public int bs_count { get; set; }
        public string gedcs_version { get; set; }

        public List<BaseStation> BaseStations { get; set; }

        public GEDCSTopology ParseGEDCSTopology(string gedcsAdminViewXML)
        {
            var topology = new GEDCSTopology();
            XDocument xmlDoc;
            try
            {
                xmlDoc = XDocument.Parse(gedcsAdminViewXML);
                var o = (from item in xmlDoc.Descendants("device_topology")
                         let bs_count = item.Attribute("bs_count")
                         where bs_count != null
                         let gedcs_version = item.Attribute("gedcs_version")
                         where gedcs_version != null
                         select
                             new
                             {
                                 bs_count = bs_count.Value,
                                 gedcs_version = gedcs_version.Value
                             }).SingleOrDefault();

                if (o != null)
                {
                    topology.bs_count = int.Parse(o.bs_count);
                    topology.gedcs_version = o.gedcs_version;

                    IEnumerable<BaseStation> baseStations = new List<BaseStation>();
                    foreach (BaseStation baseStation in GetBaseStations(gedcsAdminViewXML))
                    {
                        topology.BaseStations.Add(new BaseStation
                        {
                            BSID = baseStation.BSID
                        });
                    }

                    IEnumerable<ETag> etags = new List<ETag>();
                    foreach (BaseStation baseStation in topology.BaseStations)
                    {
                        foreach (ETag etag in GetEtags(gedcsAdminViewXML, baseStation.BSID))
                        {
                            BaseStation bs =
                                topology.BaseStations.Where(m => m.BSID.Equals(baseStation.BSID)).SingleOrDefault();

                            if (bs != null)
                                bs.ETags.Add(etag);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
            

            
            return topology;
        }

        public BaseStation GetBaseStation(string etagName)
        {
            foreach (BaseStation bs in BaseStations)
            {
                foreach (ETag etag in bs.ETags)
                {
                    if (etag.Name.Equals(etagName))
                        return bs;
                }
            }

            throw new InvalidEtagNameException("eTag not found: " + etagName);
        }

        public ETag GetEtag(string etagName)
        {
            foreach (BaseStation bs in BaseStations)
            {
                foreach (ETag etag in bs.ETags)
                {
                    if (etag.Name.Equals(etagName))
                        return etag;
                }
            }

            throw new InvalidEtagNameException("eTag not found: " + etagName);
        }

        #endregion

        private IEnumerable<BaseStation> GetBaseStations(string gedcsAdminViewXML)
        {
            XDocument xmlDoc = XDocument.Parse(gedcsAdminViewXML);
            IEnumerable<BaseStation> baseStations = new List<BaseStation>();
            baseStations = (from baseStation in xmlDoc.Descendants("bs")
                            let name = baseStation.Attribute("name")
                            where name != null
                            select
                                new BaseStation
                                    {
                                        BSID = name.Value
                                    });

            return baseStations;
        }

        private IEnumerable<ETag> GetEtags(string gedcsAdminViewXML, string BaseStationName)
        {
            IEnumerable<ETag> etags = new List<ETag>();
            XDocument xmlDoc = XDocument.Parse(gedcsAdminViewXML);

            etags = (from item in xmlDoc.Descendants("etag")
                     let parent = item.Parent
                     where parent != null
                     let xElement = parent.Parent
                     where xElement != null
                     let bsName = xElement.Attribute("name")
                     where
                         bsName != null &&
                         (xElement != null && (parent != null && bsName.Value.Equals(BaseStationName)))
                     select new ETag
                                {
                                    Name = item.Attribute("name").Value,
                                    Size = GetEtagSize(item.Descendants("panel_size").SingleOrDefault().Value)
                                });
            return etags;
        }

        private ETag.EtagSize GetEtagSize(string Size)
        {
            switch (Size)
            {
                case "S4":
                    return ETag.EtagSize.S4;
                case "S7":
                    return ETag.EtagSize.S7;
                default:
                    _logger.Error("Invalid Etag Size: " + Size);
                    throw new InvalidEtagSize("Invalid Etag Size: " + Size);
            }
        }
    }
}