﻿// -----------------------------------------------------------------------
// <copyright file="BaseStation.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Delta.GEDCS.Model.Interfaces;
using Quartz;

namespace Delta.GEDCS.Model.Device
{
    public class BaseStation : IBaseStation
    {
        public BaseStation()
        {
            ETags = new List<ETag>();
            JobDetails = new List<JobDetail>();
        }

        #region IBaseStation Members

        public string BSID { get; set; }
        public List<ETag> ETags { get; set; }
        public List<JobDetail> JobDetails { get; set; }

        #endregion
    }
}