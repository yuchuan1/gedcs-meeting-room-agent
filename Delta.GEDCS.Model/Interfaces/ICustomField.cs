﻿// -----------------------------------------------------------------------
// <copyright file="ICustomField.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Delta.GEDCS.Model.Interfaces
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ICustomField
    {
        string Name { get; set; }
        string Type { get; set; }
        string Size { get; set; }
        string Value { get; set; }
    }
}