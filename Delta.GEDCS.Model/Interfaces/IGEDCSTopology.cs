using System.Collections.Generic;
using Delta.GEDCS.Model.Device;

namespace Delta.GEDCS.Model.Interfaces
{
    public interface IGEDCSTopology
    {
        int bs_count { get; set; }
        string gedcs_version { get; set; }
        List<BaseStation> BaseStations { get; set; }
        GEDCSTopology ParseGEDCSTopology(string gedcsAdminViewXML);
        BaseStation GetBaseStation(string etagName);
        ETag GetEtag(string etagName);
    }
}