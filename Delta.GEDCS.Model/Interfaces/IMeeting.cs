using System;
using System.Collections.Generic;
using Delta.GEDCS.Client.Model;
using Delta.GEDCS.Model.Meeting;

namespace Delta.GEDCS.Model.Interfaces
{
    public interface IMeeting
    {
        string ID { get; set; }
        string RecurrenceID { get; set; }
        string MeetingRoomName { get; set; }
        string Subject { get; set; }
        string Location { get; set; }
        DateTime StartTime { get; set; }
        DateTime EndTime { get; set; }
        string Description { get; set; }
        Contact CreatedBy { get; set; }
        Contact ModifiedBy { get; set; }
        List<Contact> Attendees { get; set; }
        IEnumerable<CustomField> CustomFields { get; set; }
        IBaseStation BaseStation { get; }
        string GetXMLFilePath();
        string GetXSLTFilePath(string deviceSize);
        GEDCSApiResponse SendToGEDCS(string etagName);
        GEDCSApiResponse SendToGEDCS(string etagName, string title);
        GEDCSApiResponse SendPNGToGEDCS(string etagName, string pngFilePath);
        GEDCSApiResponse SendPNGToGEDCS(string etagName, string pngFilePath, string title);
    }
}