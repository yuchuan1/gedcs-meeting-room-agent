namespace Delta.GEDCS.Model.Device
{
    public interface IETag
    {
        string Name { get; set; }
        ETag.EtagSize Size { get; set; }
    }
}