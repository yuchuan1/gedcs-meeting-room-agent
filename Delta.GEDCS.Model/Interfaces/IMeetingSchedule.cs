using System;
using System.Collections.Generic;
using Delta.GEDCS.Model.Device;

namespace Delta.GEDCS.Model.Interfaces
{
    public interface IMeetingSchedule
    {
        GEDCSTopology Topology { get; set; }
        Request Request { get; set; }
        string Version { get; set; }
        string Type { get; set; }
        DateTime RequestTime { get; }
        IEnumerable<RequestMeetingRoomsRoomMeeting> GetMeetings(string MeetingRoomName);
        IEnumerable<RequestMeetingRoomsRoom> GetMeetingRooms();
    }
}