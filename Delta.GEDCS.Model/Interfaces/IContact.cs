﻿// -----------------------------------------------------------------------
// <copyright file="IContact.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Delta.GEDCS.Model.Meeting;

namespace Delta.GEDCS.Model.Interfaces
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IContact
    {
        string Name { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string Extension { get; set; }
        IEnumerable<CustomField> CustomFields { get; set; }
    }
}