using System.Collections.Generic;
using Delta.GEDCS.Model.Meeting;

namespace Delta.GEDCS.Model.Interfaces
{
    public interface IMeetingRoom
    {
        string Name { get; set; }
        int MeetingCount { get; }
        IList<RequestMeetingRoomsRoomMeeting> Meetings { get; set; }
        IBaseStation BaseStation { get; }
        string URL { get; set; }
        string Description { get; set; }
        IEnumerable<CustomField> CustomFields { get; set; }
        string XSLTPath { get; }
    }
}