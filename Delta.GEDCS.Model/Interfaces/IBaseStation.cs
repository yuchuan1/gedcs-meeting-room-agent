using System.Collections.Generic;
using Delta.GEDCS.Model.Device;
using Quartz;

namespace Delta.GEDCS.Model.Interfaces
{
    public interface IBaseStation
    {
        string BSID { get; set; }
        List<ETag> ETags { get; set; }
        List<JobDetail> JobDetails { get; set; }
    }
}