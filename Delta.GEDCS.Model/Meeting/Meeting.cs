﻿using Delta.GEDCS.Client.APIs;
using NLog;

namespace Delta.GEDCS.Model.Meeting
{
    public class RequestMeetingRoomsRoomMeeting
    {
        #region JobType enum

        public enum JobType
        {
            SendXMLandXSLT,
            SendPNG
        }

        #endregion

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public RequestMeetingRoomsRoomMeeting()
        {
            
        }
    }
}