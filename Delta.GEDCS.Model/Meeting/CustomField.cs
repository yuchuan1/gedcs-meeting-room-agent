﻿// -----------------------------------------------------------------------
// <copyright file="CustomField.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Model.Interfaces;

namespace Delta.GEDCS.Model.Meeting
{
    public class CustomField : ICustomField
    {
        #region ICustomField Members

        public string Name { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public string Value { get; set; }

        #endregion
    }
}