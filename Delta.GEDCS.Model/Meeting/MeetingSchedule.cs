﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Delta.GEDCS.Model.Device;
using Delta.GEDCS.Model.Interfaces;
using NLog;

namespace Delta.GEDCS.Model.Meeting
{
    public class MeetingSchedule : IMeetingSchedule
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public MeetingSchedule(string meetingInfoXml)
        {
            XmlSerializer xSerializer;
            MemoryStream stream;
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(meetingInfoXml);
                stream = new MemoryStream(byteArray);
                xSerializer = new XmlSerializer(typeof (Request));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }

            try
            {
                Request = new Request();
                Request = (Request) xSerializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                _logger.Error("Trying to deserialize xml:" + meetingInfoXml + "\n Error:" + ex.Message);
                throw;
            }
        }

        #region IMeetingSchedule Members

        public GEDCSTopology Topology { get; set; }


        public IEnumerable<RequestMeetingRoomsRoomMeeting> GetMeetings(string MeetingRoomName)
        {
            IEnumerable<RequestMeetingRoomsRoomMeeting> meetings =
                (from meetingRoom in ((RequestMeetingRooms) Request.Items[1]).Room
                 from meeting in meetingRoom.Meeting
                 where meetingRoom.name.Equals(MeetingRoomName)
                 select meeting);

            return meetings;
        }

        public Request Request { get; set; }

        public IEnumerable<RequestMeetingRoomsRoom> GetMeetingRooms()
        {
            IEnumerable<RequestMeetingRoomsRoom> rooms = (from room in ((RequestMeetingRooms) Request.Items[1]).Room
                                                          select room);
            return rooms;
        }

        public string Version { get; set; }
        public string Type { get; set; }

        public DateTime RequestTime
        {
            get { return DateTime.Now; }
        }

        #endregion

        public RequestMeetingRoomsRoomMeeting GetUpCommingMeeting(string MeetingRoomName)
        {
            RequestMeetingRoomsRoomMeeting upCommingMeeting =
                (from meetingRoom in ((RequestMeetingRooms) Request.Items[1]).Room
                 from meeting in meetingRoom.Meeting
                 where meetingRoom.name.Equals(MeetingRoomName)
                 select meeting).OrderByDescending(t => DateTime.Parse(t.TimeSlot.SingleOrDefault().startTime)).First();

            return upCommingMeeting;
        }
    }
}