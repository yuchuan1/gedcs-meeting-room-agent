﻿using System;
using System.Collections.Generic;
using Autofac;
using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Model.Device;
using Delta.GEDCS.Model.Interfaces;
using NLog;

namespace Delta.GEDCS.Model.Meeting
{
    public class MeetingRoom : IMeetingRoom
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ContainerBuilder builder = new ContainerBuilder();

        public MeetingRoom()
        {
            builder.Register(c => new APIClient()).SingleInstance();
            Meetings = new List<RequestMeetingRoomsRoomMeeting>();
        }

        #region IMeetingRoom Members

        public string Name { get; set; }

        public int MeetingCount
        {
            get { return Meetings.Count; }
        }

        public IList<RequestMeetingRoomsRoomMeeting> Meetings { get; set; }

        public IBaseStation BaseStation
        {
            get { return GetBaseStation(Name); }
        }

        public string URL { get; set; }
        public string Description { get; set; }
        public IEnumerable<CustomField> CustomFields { get; set; }


        public string XSLTPath
        {
            get { return GetXSLTPath(GetEtagSize(Name)); }
        }

        #endregion

        private string GetXSLTPath(string deviceSize)
        {
            throw new NotImplementedException();
        }

        private string GetEtagSize(string etagName)
        {
            throw new NotImplementedException();
        }

        private BaseStation GetBaseStation(string etagName)
        {
            using (IContainer container = builder.Build())
            {
                var api = container.Resolve<APIClient>();
                try
                {
                    var topology = new GEDCSTopology(api.GetAdminViewXml());
                    return topology.GetBaseStation(Name);
                }
                catch (InvalidEtagNameException ex)
                {
                    _logger.Error(ex.Message);
                    throw;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    throw;
                }
            }
        }
    }
}