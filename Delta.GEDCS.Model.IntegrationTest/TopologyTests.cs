﻿// -----------------------------------------------------------------------
// <copyright file="TopologyTests.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Model.Device;
using NUnit.Framework;

namespace Delta.GEDCS.Model.IntegrationTest
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class TopologyTests
    {
        static APIClient api = new APIClient();

        private string AdminViewXML = api.GetAdminViewXml();

        [Test]
        public void GetCorrectBaseStationID()
        {
            const string meetingRoomName = "1101";
            string expectedBaseStationID = "bs1";
            string actualBaseStationID;

            var topology = new GEDCSTopology(AdminViewXML);
            actualBaseStationID = topology.GetBaseStation(meetingRoomName).BSID;

            Assert.AreEqual(expectedBaseStationID, actualBaseStationID);
        }
    }
}