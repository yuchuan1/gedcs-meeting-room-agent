﻿using NUnit.Framework;

namespace Delta.GEDCS.Scheduler.Test
{
    [TestFixture]
    public class SchedulerTests
    {
        [Test]
        public void UpdateFrequencyIsCorrect()
        {
            var scheduler =
                new GEDCSScheduler(GEDCSScheduler.SchedulerType.SharePoint);

            Assert.AreEqual("0 0/1 * 1/1 * ? *", scheduler.GetUpdateFrequency());
        }
    }
}