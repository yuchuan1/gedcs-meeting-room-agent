﻿using System.Xml.Linq;
using Delta.GEDCS.Agent.SharePoint.Model;
using NUnit.Framework;

namespace Delta.GEDCS.Agent.SharePoint.IntegrationTest
{
    [TestFixture]
    public class MeetingRequestTests
    {
        private MeetingRequest request;

        [TestFixtureSetUp]
        public void Setup()
        {
            request = new MeetingRequest();
        }

        [Test]
        public void GetMeetingInfoXMLWorks()
        {
            string actualMeetingInfoXML = request.MeetingInfoXML;

            #region expectedXML

            const string expectedXML =
                @" <Request xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
  <Type type=""MeetingRoomReservation"" ver=""1.5"" />
  <MeetingRooms count=""2"">
    <Room name=""1101"" meetingCount=""2"">
      <Meeting subject=""meeting 1"">
        <ID>http://edcssqa/Lists/1101/DispForm.aspx?ID=2</ID>
        <Description>meeting 1 description</Description>
        <TimeSlot startTime=""2012-04-26T15:30:00+08:00"" endTime=""2012-04-26T16:30:00+08:00"" />
        <MeetingRoomName>1101</MeetingRoomName>
      </Meeting>
      <Meeting subject=""meeting 2"">
        <ID>http://edcssqa/Lists/1101/DispForm.aspx?ID=3</ID>
        <Description>meeting 2 description</Description>
        <TimeSlot startTime=""2012-04-26T17:00:00+08:00"" endTime=""2012-04-26T17:30:00+08:00"" />
        <MeetingRoomName>1101</MeetingRoomName>
      </Meeting>
    </Room>
    <Room name=""1102"" meetingCount=""2"">
      <Meeting>
        <ID>http://edcssqa/Lists/1202/DispForm.aspx?ID=2</ID>
        <Description>test meeting 4</Description>
        <TimeSlot startTime=""2012-04-26T17:30:00+08:00"" endTime=""2012-04-26T18:00:00+08:00"" />
        <MeetingRoomName>1102</MeetingRoomName>
      </Meeting>
      <Meeting>
        <ID>http://edcssqa/Lists/1202/DispForm.aspx?ID=1</ID>
        <Description>test meeting 3</Description>
        <TimeSlot startTime=""2012-04-26T17:00:00+08:00"" endTime=""2012-04-26T17:30:00+08:00"" />
        <MeetingRoomName>1102</MeetingRoomName>
      </Meeting>
    </Room>
  </MeetingRooms>
</Request>
";

            #endregion

            XElement expectedElement = XElement.Parse(expectedXML);
            XElement actualElement = XElement.Parse(actualMeetingInfoXML);
            var comparer = new XNodeEqualityComparer();
            Assert.True(comparer.Equals(actualElement, expectedElement));
        }

        [Test]
        public void GetSingleMeetingInfoXMLWorks()
        {
            Assert.True(request.GetMeetingInfoXML(@"http://edcssqa/Lists/1101/DispForm.aspx?ID=2").Contains("1101"));
        }
    }
}