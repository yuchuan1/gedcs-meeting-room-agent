using System;
using System.Collections.Generic;
using Argotic.Syndication;
using Delta.GEDCS.Agent.SharePoint.Model;

namespace Delta.GEDCS.Agent.SharePoint.Interfaces
{
    public interface IMeetingRequest
    {
        DataSource dataSource { get; set; }
        List<RssFeed> Feeds { get; set; }
        List<Field> CustomFields { get; set; }
        string MeetingInfoXML { get; set; }
        string GetMeetingInfoXML();
        string GetMeetingInfoXML(string meetingID);
        Field GetSharePointField(MeetingRequest.GEDCSDefaultField gedcsField);
        IEnumerable<RequestMeetingRoomsRoom> GetMeetingRooms();
        IEnumerable<RequestMeetingRoomsRoomMeeting> GetMeetings();
        IEnumerable<String> MeetingXMLCollection();
    }
}