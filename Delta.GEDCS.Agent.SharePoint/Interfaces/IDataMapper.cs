using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Model;

namespace Delta.GEDCS.Agent.SharePoint.Interfaces
{
    public interface IDataMapper
    {
        List<MappingFields> MappingFields { get; set; }
    }
}