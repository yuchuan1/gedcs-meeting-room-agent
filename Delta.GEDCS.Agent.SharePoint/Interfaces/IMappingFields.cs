using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Model;

namespace Delta.GEDCS.Agent.SharePoint.Interfaces
{
    public interface IMappingFields
    {
        string Type { get; set; }
        List<Field> Fields { get; set; }
    }
}