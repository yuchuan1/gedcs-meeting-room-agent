﻿using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Interfaces;

namespace Delta.GEDCS.Agent.SharePoint.Model
{
    public class DataMapper : IDataMapper
    {
        #region IDataMapper Members

        public List<MappingFields> MappingFields { get; set; }

        #endregion
    }
}