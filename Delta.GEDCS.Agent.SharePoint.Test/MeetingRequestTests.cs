﻿// -----------------------------------------------------------------------
// <copyright file="MeetingRequestTests.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using Delta.GEDCS.Agent.SharePoint.Mock.Model;
using NUnit.Framework;

namespace Delta.GEDCS.Agent.SharePoint.Test
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class MeetingRequestTests
    {
        private MeetingRequest _request;

        [TestFixtureSetUp]
        public void Setup()
        {
            _request = new MeetingRequest(
                Path.GetDirectoryName(
                    new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath) + "\\DataSource.xml");
        }

        [Test]
        public void GetMeetingInfoXMLWorks()
        {
            string actualMeetingInfoXML = _request.MeetingInfoXML;

            #region expectedXML

            const string expectedXML =
                @"<?xml version=""1.0"" encoding=""utf-8""?>
<Request xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Type type=""MeetingRoomReservation"" ver=""1.5"" />
  <MeetingRooms count=""2"">
    <Room name=""1101"" meetingCount=""1"">
      <Meeting subject=""meeting 1"">
        <MeetingRoomName>1101</MeetingRoomName>
        <ID>http://edcssqa/Lists/1101/DispForm.aspx?ID=2</ID>
        <Description>meeting 1 description</Description>
        <TimeSlot startTime=""2012-04-26T15:30:00+08:00"" endTime=""2012-04-26T16:30:00+08:00"" />
      </Meeting>
    </Room>
    <Room name=""1102"" meetingCount=""1"">
      <Meeting>
        <MeetingRoomName>1102</MeetingRoomName>
        <ID>http://edcssqa/Lists/1202/DispForm.aspx?ID=1</ID>
        <Description>test meeting 3</Description>
        <TimeSlot startTime=""2012-04-26T17:00:00+08:00"" endTime=""2012-04-26T17:30:00+08:00"" />
      </Meeting>
    </Room>
  </MeetingRooms>
</Request>";

            #endregion

            XElement expectedElement = XElement.Parse(expectedXML);
            XElement actualElement = XElement.Parse(actualMeetingInfoXML);
            var comparer = new XNodeEqualityComparer();
            Assert.True(comparer.Equals(actualElement, expectedElement));
        }


        [Test]
        public void GetSingleMeetingInfoXMLWorks()
        {
            string expectedXML =
                @"<?xml version=""1.0"" encoding=""utf-8""?>
<Request xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Type type=""MeetingRoomReservation"" ver=""1.5"" />
  <MeetingRooms count=""1"">
    <Room name=""1101"" meetingCount=""1"">
      <Meeting subject=""meeting 1"">
        <MeetingRoomName>1101</MeetingRoomName>
        <ID>http://edcssqa/Lists/1101/DispForm.aspx?ID=2</ID>
        <Description>meeting 1 description</Description>
        <TimeSlot startTime=""2012-04-26T15:30:00+08:00"" endTime=""2012-04-26T16:30:00+08:00"" />
      </Meeting>
    </Room>
  </MeetingRooms>
</Request>";
            string acutalXML = _request.GetMeetingInfoXML(@"http://edcssqa/Lists/1101/DispForm.aspx?ID=2");

            XElement expectedElement = XElement.Parse(expectedXML);
            XElement actualElement = XElement.Parse(acutalXML);
            var comparer = new XNodeEqualityComparer();
            Assert.True(comparer.Equals(actualElement, expectedElement));
        }
    }
}