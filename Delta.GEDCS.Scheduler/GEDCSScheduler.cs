﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using Delta.GEDCS.Jobs;
using NLog;
using Quartz;
using Quartz.Impl;

namespace Delta.GEDCS
{
    public class GEDCSScheduler
    {
        #region SchedulerType enum

        public enum SchedulerType
        {
            SharePoint,
            Exchange,
            GoogleCalendar
        }

        #endregion

        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly ISchedulerFactory schedFact = new StdSchedulerFactory();

        private readonly string CurrentDir =
            Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

        private readonly IScheduler sched = schedFact.GetScheduler();

        private JobMonitor jobMonitor = JobMonitor.Instance;

        public GEDCSScheduler(SchedulerType schedulerType)
        {
            // initialize scheduler
            Type = schedulerType;
            Trigger sharePointJobTrigger;
            _logger.Debug("Creating a new SharePoint job");
            var sharepointJobDetail = new JobDetail("sharepointJob", "GEDCS", typeof (SendToGEDCSJob));
            sharepointJobDetail.JobDataMap.Put("JobMonitor", jobMonitor);

            _logger.Debug("Creating new cron trigger with update frequency from:" + CurrentDir);
            sharePointJobTrigger = new CronTrigger("mySharepointTrigger", "GEDCS", GetUpdateFrequency())
                                       {
                                           StartTimeUtc = TriggerUtils.GetEvenSecondDate(DateTime.UtcNow),
                                           Name = "GEDCS Sharepoint Trigger"
                                       };

            _logger.Debug("Scheduling SharePoint job ");
            sched.ScheduleJob(sharepointJobDetail, sharePointJobTrigger);
        }

        public SchedulerType Type { get; set; }

        public string GetUpdateFrequency()
        {
            return ConfigurationManager.AppSettings["UpdateFrequency"];
        }

        public void Start()
        {
            // Check app.config for the interval 
            // Start a task for data source polling
            _logger.Debug("Starting scheduler...");
            sched.Start();
        }

        public void Stop()
        {
            _logger.Debug("Shutting down scheduler...");
            sched.Shutdown();
        }

        public void Pause()
        {
            sched.PauseAll();
            _logger.Debug("Scheduler paused...");
        }

        public void Resume()
        {
            sched.ResumeAll();
            _logger.Debug("Scheduler resumed...");
        }

        public void Shutdown()
        {
            _logger.Debug("Shutting down scheduler...");
            sched.Shutdown();
        }
    }
}