﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;
using Delta.GEDCS.Agent.SharePoint.Model;
using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Model;
using Delta.GEDCS.Model.Device;
using Delta.GEDCS.Model.Meeting;
using NLog;

namespace Delta.GEDCS.Jobs
{
    public class Test
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string CurrentDir =
            Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

        private readonly string S4_EtagFileName = ConfigurationManager.AppSettings["S4_EtagXSLTFileName"];
        private readonly string S7_EtagFileName = ConfigurationManager.AppSettings["S7_EtagXSLTFileName"];

        private readonly APIClient api = new APIClient();

        public void TestRun(JobMonitor jobMonitor)
        {
            var gedcsApiResponse = new GEDCSApiResponse();
            // Retrieve current meeting requests from sharepoint server
            _logger.Debug("Initialize MeetingRequest to retrieve current meeting requests from sharepoint server...");
            var request = new MeetingRequest();
            _logger.Debug("\n" + request.MeetingInfoXML);

            // Construct meeting schedule
            _logger.Debug("Initialize MeetingSchedule...");
            var schedule = new MeetingSchedule(request.MeetingInfoXML);

            //Construct topology according to admin view
            _logger.Debug("Initialize topology");

//            #region define AdminViewXML

//            string AdminViewXML =
//                @"<?xml version=""1.0"" encoding=""utf-8""?>
//            <device_topology bs_count=""3"" gedcs_version=""1.1.1"">
//                <bs name=""bs1"" >
//                
//                    <bs_ip>192.168.0.1</bs_ip>
//                    <bs_mac>fe80::227:10ff:fe55:fbdc</bs_mac>
//                    <bs_channel>10</bs_channel>   
//                    <etags count=""2"">                    
//                        <etag name=""1101"">
//                            <panel_size>S7</panel_size>
//                            <etag_id>0x000001</etag_id>
//                        </etag>
//                        <etag name=""1102"">
//                            <panel_size>S7</panel_size>
//                            <etag_id>0x000002</etag_id>
//                        </etag>
//                    </etags> 
//                </bs>
//                <bs name=""bs2"">
//                    <bs_ip>192.168.0.2</bs_ip>
//                    <bs_mac>fe80::227:10ff:xxxx:fbdc</bs_mac>
//                    <bs_channel>11</bs_channel>
//                    <etags count=""2"">
//                        <etag name=""1201"">
//                            <panel_size>S4</panel_size>
//                            <etag_id>0x000003</etag_id>
//                        </etag>
//                        <etag name=""1202"">
//                            <panel_size>S4</panel_size>
//                            <etag_id>0x000004</etag_id>
//                        </etag>
//                    </etags>
//                </bs>
//                <bs name=""bs3"">
//                    <bs_ip>192.168.0.3</bs_ip>
//                    <bs_mac>fe80::227:10ff:xxxx:fbxx</bs_mac>
//                    <bs_channel>12</bs_channel>
//                    <etags count=""3"">
//                        <etag name=""1301"">
//                            <panel_size>S7</panel_size>
//                            <etag_id>0x000005</etag_id>
//                        </etag>
//                        <etag name=""1302"">
//                            <panel_size>S4</panel_size>
//                            <etag_id>0x000000</etag_id>
//                        </etag>
//                        <etag name=""1303"">
//                            <panel_size>S7</panel_size>
//                            <etag_id>0x000000</etag_id>
//                        </etag>
//                    </etags>
//                </bs>
//            </device_topology>
//            ";

//            #endregion

            string AdminViewXML = api.GetAdminViewXml();
            string logAdminViewXML = AdminViewXML.Replace(">", ">" + Environment.NewLine);
            logAdminViewXML = logAdminViewXML.Replace("<", Environment.NewLine + "<");

            _logger.Debug("Topology: \n" + logAdminViewXML);

            var topology = new GEDCSTopology(AdminViewXML);

            IEnumerable<RequestMeetingRoomsRoom> rooms = request.GetMeetingRooms();
            foreach (RequestMeetingRoomsRoom meetingRoom in rooms)
            {
                foreach (RequestMeetingRoomsRoomMeeting meeting in meetingRoom.Meeting)
                {
                    string xml = request.GetMeetingInfoXML(meeting.ID);
                    // if meetingXML contains meeting information then send else skip
                    if (xml.Contains("<Meeting "))
                    {
                        byte[] xmlByteArray = Encoding.UTF8.GetBytes(xml);
                        string xsltPath = string.Empty;
                        ETag etag = topology.GetEtag(meetingRoom.name);
                        switch (etag.Size)
                        {
                            case ETag.EtagSize.S4:
                                xsltPath = CurrentDir + "\\" + S4_EtagFileName;
                                break;
                            case ETag.EtagSize.S7:
                                xsltPath = CurrentDir + "\\" + S7_EtagFileName;
                                break;
                            default:
                                throw new InvalidEtagSize("Etag size not found: " + etag.Size);
                        }

                        _logger.Debug("Sending image to GEDCS server. Meeting Room: " + meetingRoom.name);
                        //try
                        //{
                        //    gedcsApiResponse = new GEDCSApiResponse();
                        //    gedcsApiResponse.JobID = "FakeJobID";
                        //    gedcsApiResponse.MessageCode = "20001";
                        //}
                        //catch (Exception ex)
                        //{
                        //    _logger.Warn(ex.ToString());
                        //}

                        //jobsToMonitorModel.JobsToMonitor.Add(gedcsApiResponse);
                        try
                        {
                            gedcsApiResponse = api.SendXMLandXSLTToEtag(meetingRoom.name, xmlByteArray,
                                                                        xsltPath);
                            _logger.Debug("Respose: " + gedcsApiResponse.MessageCode + ", " +
                                          gedcsApiResponse.MessageDescription);

                            _logger.Debug("Adding job \"" + gedcsApiResponse.JobID + "\" to job monitor list");
                            //jobsToMonitorModel.JobsToMonitor.Add(gedcsApiResponse);
                            jobMonitor.AddJob(gedcsApiResponse);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.ToString());
                        }
                        
                    }
                }
            }
        }

       
    }
}