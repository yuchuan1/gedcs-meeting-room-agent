﻿// -----------------------------------------------------------------------
// <copyright file="SendToGEDCSJob.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Delta.GEDCS.Agent.SharePoint.Model;
using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Model;
using Delta.GEDCS.Model.Device;
using Delta.GEDCS.Model.Meeting;
using NLog;
using Quartz;

namespace Delta.GEDCS.Jobs
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SendToGEDCSJob : IJob
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string CurrentDir =
            Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

        private readonly string S4_EtagFileName = ConfigurationManager.AppSettings["S4_EtagXSLTFileName"];
        private readonly string S7_EtagFileName = ConfigurationManager.AppSettings["S7_EtagXSLTFileName"];

        private readonly APIClient api = new APIClient();
        private GEDCSApiResponse gedcsApiResponse = new GEDCSApiResponse();
        private JobMonitor jobMonitor = JobMonitor.Instance;

        #region IJob Members

        public void Execute(JobExecutionContext context)
        {
            string conn = "Data Source=ScheduleDB.sdf;Persist Security Info=False;";
            ScheduleDB db = new ScheduleDB(conn);
            
            // Retrieve current meeting requests from sharepoint server
            _logger.Debug("Initialize MeetingRequest to retrieve current meeting requests from sharepoint server...");
            var request = new MeetingRequest();
            _logger.Debug(request.MeetingInfoXML);

            // Construct meeting schedule
            _logger.Debug("Initialize MeetingSchedule...");
            var schedule = new MeetingSchedule(request.MeetingInfoXML);

            //Construct topology according to admin view
            _logger.Debug("Initialize topology");

            string AdminViewXML = api.GetAdminViewXml();

            var topology = new GEDCSTopology(AdminViewXML);

            IEnumerable<RequestMeetingRoomsRoom> rooms = request.GetMeetingRooms();
            foreach (RequestMeetingRoomsRoom meetingRoom in rooms)
            {
                Parallel.ForEach(meetingRoom.Meeting, meeting =>
                {
                    string xml = request.GetMeetingInfoXML(meeting.ID);

                    if (xml.Contains("<Meeting "))
                    {
                        byte[] xmlByteArray = Encoding.UTF8.GetBytes(xml);
                        string xsltPath = string.Empty;
                        ETag etag = topology.GetEtag(meetingRoom.name);
                        switch (etag.Size)
                        {
                            case ETag.EtagSize.S4:
                                xsltPath = CurrentDir + "\\" + S4_EtagFileName;
                                break;
                            case ETag.EtagSize.S7:
                                xsltPath = CurrentDir + "\\" + S7_EtagFileName;
                                break;
                            default:
                                _logger.Error("Etag size not found: " + etag.Size);
                                throw new InvalidEtagSize("Etag size not found: " + etag.Size);
                        }
                        _logger.Debug("MeetingXML for meeting id: " + meeting.ID + " \n " + xml);
                        _logger.Debug("Sending image to GEDCS server. Meeting Room: " + meetingRoom.name);

                        //GEDCSApiResponse gedcsApiResponse = new GEDCSApiResponse();
                        try
                        {
                            int count = (from i in db.Meeting
                                       where i.MeetingRoomName.Equals(meetingRoom.name)
                                       && i.Xml.Equals(xml)
                                       select i).Count();

                            if (count == 0)
                            {
                                gedcsApiResponse = api.SendXMLandXSLTToEtag(meetingRoom.name, xmlByteArray,
                                                                            xsltPath);

                                if (gedcsApiResponse.JobID.Trim() != string.Empty)
                                {
                                    // save xml into memory or database
                                    count = (from i in db.Meeting
                                             where i.JobId.Equals(gedcsApiResponse.JobID.Trim())
                                             select i).Count();

                                    if (count == 0)
                                    {
                                        Meeting m = new Meeting();
                                        m.JobId = gedcsApiResponse.JobID;
                                        m.MeetingRoomName = meetingRoom.name;
                                        m.Sent = false;
                                        m.Xml = xml;
                                        m.CreatedTime = DateTime.Now;

                                        db.Meeting.InsertOnSubmit(m);
                                        db.SubmitChanges();
                                    }
                                }

                                _logger.Debug("Respose: " + gedcsApiResponse.MessageCode + ", " +
                                        gedcsApiResponse.MessageDescription);

                                _logger.Debug("Adding job \"" + gedcsApiResponse.JobID + "\" to job monitor list");

                                JobDataMap dataMap = context.JobDetail.JobDataMap;

                                jobMonitor = (JobMonitor)dataMap.Get("JobMonitor");
                                if (!String.IsNullOrEmpty(gedcsApiResponse.JobID.Trim()))
                                {
                                    // Check if the job has been sent before
                                    jobMonitor.AddJob(gedcsApiResponse);
                                }
                                else
                                    _logger.Error("JobId is empty!");

                            }
                            else
                            {
                                _logger.Debug("Image had been sent before, skipping...");
                            }

                            
                        }
                        catch (Exception ex)
                        {
                            _logger.Warn(ex.ToString());
                        }
                    }
                });

                //foreach (RequestMeetingRoomsRoomMeeting meeting in meetingRoom.Meeting)
                //{
                //    string xml = request.GetMeetingInfoXML(meeting.ID);
                //    if (xml.Contains("<Meeting "))
                //    {
                //        byte[] xmlByteArray = Encoding.UTF8.GetBytes(xml);
                //        string xsltPath = string.Empty;
                //        ETag etag = topology.GetEtag(meetingRoom.name);
                //        switch (etag.Size)
                //        {
                //            case ETag.EtagSize.S4:
                //                xsltPath = CurrentDir + "\\" + S4_EtagFileName;
                //                break;
                //            case ETag.EtagSize.S7:
                //                xsltPath = CurrentDir + "\\" + S7_EtagFileName;
                //                break;
                //            default:
                //                _logger.Error("Etag size not found: " + etag.Size);
                //                throw new InvalidEtagSize("Etag size not found: " + etag.Size);
                //        }
                //        _logger.Debug("MeetingXML for meeting id: " + meeting.ID + " \n " + xml);
                //        _logger.Debug("Sending image to GEDCS server. Meeting Room: " + meetingRoom.name);

                //        //GEDCSApiResponse gedcsApiResponse = new GEDCSApiResponse();
                //        try
                //        {
                //            gedcsApiResponse = api.SendXMLandXSLTToEtag(meetingRoom.name, xmlByteArray,
                //                                                        xsltPath);
                //            _logger.Debug("Respose: " + gedcsApiResponse.MessageCode + ", " +
                //                          gedcsApiResponse.MessageDescription);

                //            _logger.Debug("Adding job \"" + gedcsApiResponse.JobID + "\" to job monitor list");

                //            JobDataMap dataMap = context.JobDetail.JobDataMap;
                //            jobMonitor = (JobMonitor)dataMap.Get("JobMonitor");
                //            jobMonitor.AddJob(gedcsApiResponse);
                //        }
                //        catch (Exception ex)
                //        {
                //            _logger.Warn(ex.ToString());
                //        }
                //    }
                //}
            }
        }

        #endregion
    }
}