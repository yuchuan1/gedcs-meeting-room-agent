﻿// -----------------------------------------------------------------------
// <copyright file="JobsToMonitorModel.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using Delta.GEDCS.Client.Model;

namespace Delta.GEDCS.Jobs
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class JobsToMonitorModel
    {

        public JobsToMonitorModel()
        {
            ModifiedItems = new ConcurrentBag<GEDCSApiResponse>();

            JobsToMonitor = new ObservableCollection<GEDCSApiResponse>();
            JobsToMonitor.CollectionChanged += OnCollectionChanged;
        }

        public ObservableCollection<GEDCSApiResponse> JobsToMonitor { get; set; }
        public ConcurrentBag<GEDCSApiResponse> ModifiedItems { get; set; }
        public ConcurrentBag<string> SentJobs { get; set; }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                foreach (GEDCSApiResponse newItem in e.NewItems)
                {
                    ModifiedItems.Add(newItem);

                    //Add listener for each item on PropertyChanged event
                    newItem.PropertyChanged += OnItemPropertyChanged;
                }

            if (e.OldItems != null)
                foreach (GEDCSApiResponse oldItem in e.OldItems)
                {
                    ModifiedItems.Add(oldItem);

                    oldItem.PropertyChanged -= OnItemPropertyChanged;
                }
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var item = sender as GEDCSApiResponse;
            if (item != null)
                ModifiedItems.Add(item);
        }
    }
}