﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Model;
using NLog;

namespace Delta.GEDCS.Jobs
{
    public sealed class JobMonitor
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private static volatile JobMonitor instance;
        private static readonly object syncRoot = new Object();
        private JobsToMonitorModel _jobsToMonitorModel;
        private readonly APIClient api = new APIClient();

        private JobMonitor()
        {
            _jobsToMonitorModel = new JobsToMonitorModel();
            ((INotifyCollectionChanged)_jobsToMonitorModel.JobsToMonitor).CollectionChanged += JobsToMonitorCollectionChanged;
        }

        public static JobMonitor Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new JobMonitor();
                    }
                }

                return instance;
            }
        }

        private void JobsToMonitorCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            int jobMonitorFrequency = GetJobMonitorFrequency();
            var taskJobMonitor = new Task(() => MonitorJobs());
            taskJobMonitor.Wait(jobMonitorFrequency);
            taskJobMonitor.Start();
        }

        public void MonitorJobs()
        {
            _logger.Debug("Start monitoring jobs...");
            string conn = "Data Source=ScheduleDB.sdf;Persist Security Info=False;";
            ScheduleDB db = new ScheduleDB(conn);

            while (true)
            {
                Thread.Sleep(GetJobMonitorFrequency());

                var oldMeetings = from i in db.Meeting
                                  where i.CreatedTime.Value.Date < DateTime.Now.Date
                                  select i;


                foreach (Meeting oldMeeting in oldMeetings)
                {
                    db.Meeting.DeleteOnSubmit(oldMeeting);   
                }
                db.SubmitChanges();

                _logger.Debug("Job count= " + _jobsToMonitorModel.JobsToMonitor.Count.ToString());
                try
                {
                    foreach (GEDCSApiResponse job in _jobsToMonitorModel.JobsToMonitor)
                    {
                        _logger.Debug("Before calling Job Status API");
                        _logger.Debug(" Job ID: " + job.JobID);
                        _logger.Debug(" MessageCode: " + job.MessageCode);
                        _logger.Debug(" MessageDescription:" + job.MessageDescription);
                        _logger.Debug("Calling job status API...");
                        try
                        {
                            GEDCSApiResponse currentJob = api.GetJobResult(job.JobID);
                            _logger.Debug("After calling Job Status API");
                            _logger.Debug(" Job ID: " + currentJob.JobID);
                            _logger.Debug(" MessageCode: " + currentJob.MessageCode);
                            _logger.Debug(" MessageDescription:" + currentJob.MessageDescription);
                            _logger.Debug(" Job Status:" + currentJob.JobStatus);

                            if (!currentJob.MessageCode.Equals("20001") && !currentJob.MessageCode.Equals("20002"))
                            {
                                _logger.Debug("Job Status code: " + currentJob.MessageCode);
                                _logger.Debug("Job Status: " + currentJob.MessageDescription);
                                _logger.Debug("Removing job \"" + currentJob.JobID + "\" from job monitor list");
                                _jobsToMonitorModel.JobsToMonitor.Remove(job);

                                //Meeting temp = (from i in db.Meeting
                                //           where i.JobId.Equals(job.JobID)
                                //           select i).SingleOrDefault();
                                
                                //if(temp!=null)
                                //    db.Meeting.DeleteOnSubmit(temp);

                                break;
                            }
                        }
                        catch (HttpException ex)
                        {
                            _logger.Error(ex.Message);
                        }
                        catch (InvalidOperationException ex)
                        {
                            // expected exception, intentionally left empty
                            _logger.Debug("Monitor job list changed, restart monitoring loop...");
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.Message);
                        }
                    }
                }
                catch (InvalidOperationException ex)
                {
                    // expected exception, intentionally left empty
                    _logger.Debug("File change detected, restart monitoring loop...");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }
            }
        }

        private int GetJobMonitorFrequency()
        {
            try
            {
                return int.Parse(ConfigurationManager.AppSettings["JobMoniorInterval"]);
            }
            catch (Exception ex)
            {
                _logger.Error("Failed getting JobMonitorInterval: " + ex.Message);
                throw new ArgumentException("Failed getting JobMonitorInterval: " + ex.Message);
            }
            
        }

        public void AddJob(GEDCSApiResponse job)
        {
                _jobsToMonitorModel.JobsToMonitor.Add(job);
            
        }
        public void RemoveJob(GEDCSApiResponse job)
        {
            _jobsToMonitorModel.JobsToMonitor.Remove(job);
        }
    }
}