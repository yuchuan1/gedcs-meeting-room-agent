﻿// -----------------------------------------------------------------------
// <copyright file="MeetingRequest.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using NLog;

namespace Delta.GEDCS.Agent.Exchange.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MeetingRequest
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string CurrentDir =
            Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

        private List<RequestTypeReservationRoomRecord> meetings = new List<RequestTypeReservationRoomRecord>();

        public Request request { get; set; }

        /// <summary>
        /// Takes Meeintg request XML format v1.4 from MS Exchange component
        /// Converts it into common GEDCS xml format and use it to return MeetingInfoXML
        /// </summary>
        /// <param name="metingRequestXml"></param>
        public MeetingRequest(string dataSourceXMLFilePath)
        {
            _logger.Debug("Initializing MeetingRequest object with " + dataSourceXMLFilePath + " ...");
            var stream = new StreamReader(dataSourceXMLFilePath);
            var xSerializer = new XmlSerializer(typeof(Request));
            request = new Request();
            request = (Request)xSerializer.Deserialize(stream);
            //MeetingInfoXML = GetMeetingInfoXML();
        }
    }
}
