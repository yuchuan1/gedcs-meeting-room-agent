﻿// -----------------------------------------------------------------------
// <copyright file="MeetingRequest.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using Argotic.Syndication;
using Delta.GEDCS.Agent.SharePoint.Mock.Interfaces;
using Delta.GEDCS.Model;
using NLog;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Model
{
    /// <summary>
    /// Provide mechanism for converting SharePoint RSS feeds into MeetingInfo.xml string
    /// </summary>
    public class MeetingRequest : IMeetingRequest
    {
        #region GEDCSDefaultField enum

        public enum GEDCSDefaultField
        {
            Subject,
            StartTime,
            EndTime,
            Description,
            CreatedBy
        }

        #endregion

        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string CurrentDir =
            Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

        private string _sharePointCreatedBy;
        private string _sharePointDescription;
        private string _sharePointEndTime;
        private string _sharePointStartTime;
        private string _sharePointSubject;

        public MeetingRequest()
        {
            _logger.Debug("Opening " + CurrentDir + "\\DataSource.xml");
            string dataSourceXMLFile = CurrentDir + "\\DataSource.xml";
            if (File.Exists(dataSourceXMLFile))
                Initialize(dataSourceXMLFile);
            else
            {
                _logger.Fatal("File not found: " + dataSourceXMLFile);
                throw new FileNotFoundException("File not found: " + dataSourceXMLFile);
            }
        }

        public MeetingRequest(string dataSourceXML)
        {
            _logger.Debug("Opening " + dataSourceXML);
            string dataSourceXMLFile = dataSourceXML;
            if (File.Exists(dataSourceXMLFile))
                Initialize(dataSourceXMLFile);
            else
            {
                _logger.Fatal("File not found: " + dataSourceXMLFile);
                throw new FileNotFoundException("File not found: " + dataSourceXMLFile);
            }
        }

        #region IMeetingRequest Members

        public List<Field> CustomFields
        {
            get { return GetCustomFields(); }
            set { }
        }

        public DataSource dataSource { get; set; }

        // create a memory storage for storing rss fees
        public List<RssFeed> Feeds { get; set; }
        public string MeetingInfoXML { get; set; }

        public string GetMeetingInfoXML()
        {
            var xSerializer = new XmlSerializer(typeof (Request));
            var list = new List<object>();
            var request = new Request();

            var type = new RequestType {type = "MeetingRoomReservation", ver = "1.5"};
            list.Add(type);

            var meetingRooms = new RequestMeetingRooms();
            DataSourceSharePointCalendar dataSourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
            if (dataSourceSharePointCalendar != null)
                meetingRooms.count = dataSourceSharePointCalendar.SharePointDataSource.Count().ToString();


            var listOfRooms = new List<RequestMeetingRoomsRoom>();
            for (int i = 0; i < int.Parse(meetingRooms.count); i++)
            {
                var room = new RequestMeetingRoomsRoom();
                DataSourceSharePointCalendar sourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
                if (sourceSharePointCalendar != null)
                    room.name = sourceSharePointCalendar.SharePointDataSource[i].name;

                List<RequestMeetingRoomsRoomMeeting> meetings = GetMeetings(room.name);
                room.Meeting = meetings.ToArray();
                room.meetingCount = room.Meeting.Count().ToString();
                listOfRooms.Add(room);
            }

            meetingRooms.Room = listOfRooms.ToArray();
            list.Add(meetingRooms);
            request.Items = list.ToArray();

            var sb = new StringBuilder();
            var writer = new StringWriterWithEncoding(sb, Encoding.UTF8);
            xSerializer.Serialize(writer, request);
            return sb.ToString();
        }

        public string GetMeetingInfoXML(string meetingID)
        {
            bool addToOutput = false;
            var xSerializer = new XmlSerializer(typeof (Request));
            var list = new List<object>();
            var request = new Request();

            var type = new RequestType();
            type.type = "MeetingRoomReservation";
            type.ver = "1.5";
            list.Add(type);

            var meetingRooms = new RequestMeetingRooms();
            DataSourceSharePointCalendar dataSourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
            if (dataSourceSharePointCalendar != null)
                meetingRooms.count = "1";

            var listOfRooms = new List<RequestMeetingRoomsRoom>();
            for (int i = 0; i < int.Parse(meetingRooms.count); i++)
            {
                var room = new RequestMeetingRoomsRoom();
                DataSourceSharePointCalendar sourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
                if (sourceSharePointCalendar != null)
                    room.name = sourceSharePointCalendar.SharePointDataSource[i].name;

                var meetings = new List<RequestMeetingRoomsRoomMeeting>();
                RequestMeetingRoomsRoomMeeting tempMeeting = GetMeetingToSend(GetMeetings(room.name), room.name);
                if (tempMeeting != null)
                {
                    meetings.Add(tempMeeting);
                }

                foreach (RequestMeetingRoomsRoomMeeting requestMeetingRoomsRoomMeeting in meetings)
                {
                    if (requestMeetingRoomsRoomMeeting.ID.Equals(meetingID))
                    {
                        listOfRooms.Add(room);
                        meetingRooms.Room = listOfRooms.ToArray();
                        list.Add(meetingRooms);
                        request.Items = list.ToArray();
                    }
                }
            }
            var sb = new StringBuilder();
            var writer = new StringWriterWithEncoding(sb, Encoding.UTF8);
            xSerializer.Serialize(writer, request);
            return sb.ToString();
        }

        // parse each RSS feed to generate Meeting Object

        public IEnumerable<RequestMeetingRoomsRoom> GetMeetingRooms()
        {
            var meetingRooms = new RequestMeetingRooms();
            DataSourceSharePointCalendar dataSourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
            if (dataSourceSharePointCalendar != null)
                meetingRooms.count = dataSourceSharePointCalendar.SharePointDataSource.Count().ToString();

            var listOfRooms = new List<RequestMeetingRoomsRoom>();
            for (int i = 0; i < int.Parse(meetingRooms.count); i++)
            {
                var room = new RequestMeetingRoomsRoom();
                DataSourceSharePointCalendar sourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
                if (sourceSharePointCalendar != null)
                    room.name = sourceSharePointCalendar.SharePointDataSource[i].name;

                List<RequestMeetingRoomsRoomMeeting> meetings = GetMeetings(room.name);
                room.Meeting = meetings.ToArray();
                room.meetingCount = room.Meeting.Count().ToString();
                listOfRooms.Add(room);
            }
            return listOfRooms;
        }

        public IEnumerable<RequestMeetingRoomsRoomMeeting> GetMeetings()
        {
            var list = new List<RequestMeetingRoomsRoomMeeting>();
            foreach (RequestMeetingRoomsRoom room in GetMeetingRooms())
            {
                foreach (RequestMeetingRoomsRoomMeeting meeintg in room.Meeting)
                {
                    list.Add(meeintg);
                }
            }
            return list;
        }

        public IEnumerable<String> MeetingXMLCollection()
        {
            var xmls = new List<string>();
            foreach (RequestMeetingRoomsRoomMeeting meeting in GetMeetings())
            {
                xmls.Add(GetMeetingInfoXML(meeting.ID));
            }
            return xmls;
        }

        public Field GetSharePointField(GEDCSDefaultField gedcsField)
        {
            Field field;
            switch (gedcsField)
            {
                case GEDCSDefaultField.Subject:
                    return (from item in CustomFields
                            where item.GEDCSName.Equals("subject")
                            select item).SingleOrDefault();

                case GEDCSDefaultField.StartTime:
                    return (from item in CustomFields
                            where item.GEDCSName.Equals("startTime")
                            select item).SingleOrDefault();

                case GEDCSDefaultField.EndTime:
                    return (from item in CustomFields
                            where item.GEDCSName.Equals("endTime")
                            select item).SingleOrDefault();

                case GEDCSDefaultField.Description:
                    return (from item in CustomFields
                            where item.GEDCSName.Equals("description")
                            select item).SingleOrDefault();

                case GEDCSDefaultField.CreatedBy:
                    return (from item in CustomFields
                            where item.GEDCSName.Equals("createdBy")
                            select item).SingleOrDefault();
                default:
                    _logger.Debug("This field does not belong to the GEDCS default fields." + gedcsField.ToString());
                    throw new Exception("This field does not belong to the GEDCS default fields." +
                                        gedcsField.ToString());
            }
        }

        #endregion

        private List<Field> GetCustomFields()
        {
            string mappingXMLFile = CurrentDir + "\\DataMapper.xml";
            if (File.Exists(mappingXMLFile))
            {
                XDocument xdoc = XDocument.Load(mappingXMLFile);
                ParallelQuery<Field> fields = (from dataMapper in xdoc.Descendants("DataMapper").AsParallel()
                                               from mappingField in dataMapper.Descendants("MappingFields").AsParallel()
                                               from field in mappingField.Descendants("Field").AsParallel()
                                               where mappingField.Attribute("type").Value.Equals("SharePointCalendar")
                                               select new Field
                                                          {
                                                              GEDCSName = field.Attribute("GEDCSName").Value,
                                                              SharePointName = field.Attribute("SharePointName").Value,
                                                              Type = field.Attribute("type").Value,
                                                              Size = field.Attribute("size").Value
                                                          });
                return fields.ToList();
            }
            _logger.Fatal("File not found: " + mappingXMLFile);
            throw new FileNotFoundException("File not found: " + mappingXMLFile);
        }

        private void Initialize(string dataSourceXMLFilePath)
        {
            _logger.Debug("Initializing MeetingRequest object");
            var stream = new StreamReader(dataSourceXMLFilePath);
            var xSerializer = new XmlSerializer(typeof (DataSource));

            // Parse DataSource.xml into memory
            dataSource = new DataSource();
            dataSource = (DataSource) xSerializer.Deserialize(stream);
            Feeds = new List<RssFeed>();

            _sharePointSubject = GetSharePointField(GEDCSDefaultField.Subject).SharePointName;
            _sharePointDescription = GetSharePointField(GEDCSDefaultField.Description).SharePointName;
            _sharePointCreatedBy = GetSharePointField(GEDCSDefaultField.CreatedBy).SharePointName;
            _sharePointEndTime = GetSharePointField(GEDCSDefaultField.EndTime).SharePointName;
            _sharePointStartTime = GetSharePointField(GEDCSDefaultField.StartTime).SharePointName;

            CustomFields = GetCustomFields();
            MeetingInfoXML = GetMeetingInfoXML();
        }

        // Parse DataSource.xml and returns a list of RSS Feeds
        private IEnumerable<RssFeed> GetFeeds(string meetingRoomName)
        {
            var feeds = new List<RssFeed>();
            DataSourceSharePointCalendar dataSourceSharePointCalendar = dataSource.SharePointCalendar.SingleOrDefault();
            if (dataSourceSharePointCalendar != null)
                foreach (
                    DataSourceSharePointCalendarSharePointDataSource item in
                        dataSourceSharePointCalendar.SharePointDataSource)
                {
                    if (item.name.Equals(meetingRoomName))
                    {
                        var uri = new Uri(item.URL);
                        DataSourceSharePointCalendarSharePointDataSourceSharePoint sharePoint =
                            item.SharePoint.SingleOrDefault();
                        if (sharePoint != null)
                        {
                            try
                            {
                                _logger.Debug("Getting RSS feed: " + uri);
                                RssFeed feed = RssFeed.Create(uri,
                                                              new NetworkCredential(sharePoint.user,
                                                                                    sharePoint.password),
                                                              null);
                                feeds.Add(feed);
                            }
                            catch (WebException ex)
                            {
                                _logger.Fatal(ex.Message);
                                throw;
                            }
                        }
                    }
                }
            return feeds;
        }

        private List<RequestMeetingRoomsRoomMeeting> GetMeetings(string meetingRoomName)
        {
            var meetings = new List<RequestMeetingRoomsRoomMeeting>();
            _logger.Info("Start parsing sharepoint feed...");
            _logger.Info("Matching feed with meeting room: " + meetingRoomName);
            string tempStartTime = string.Empty;
            foreach (RssFeed rssFeed in GetFeeds(meetingRoomName))
            {
                foreach (RssItem item in rssFeed.Channel.Items)
                {
                    _logger.Info("Start parsing sharepoint feed for meeting room name:" + meetingRoomName);
                    string xml = item.Description;
                    // In order to form a valid xml format where the root element must be unique, we add the following line
                    xml = "<description>" + xml + "</description>";
                    var meeting = new RequestMeetingRoomsRoomMeeting();
                    meeting.ID = item.Link.ToString();
                    XDocument xmlDoc = XDocument.Parse(xml);
                    var fields = (from element in xmlDoc.Descendants("div").AsParallel()
                                  select new
                                             {
                                                 element.Value
                                             });

                    var timeSlot = new RequestMeetingRoomsRoomMeetingTimeSlot();
                    var slots = new List<RequestMeetingRoomsRoomMeetingTimeSlot>();

                    foreach (var field in fields)
                    {
                        if (field.Value.Contains(_sharePointStartTime))
                        {
                            tempStartTime = DateTime.Parse(
                                field.Value.Replace(_sharePointStartTime + ":", string.Empty).Trim()).
                                ToString(
                                    "yyyy-MM-ddTHH':'mm':'sszzz");
                        }
                        if (field.Value.Contains(_sharePointEndTime))
                        {
                            timeSlot.endTime =
                                DateTime.Parse(field.Value.Replace(_sharePointEndTime + ":", string.Empty).Trim()).
                                    ToString(
                                        "yyyy-MM-ddTHH':'mm':'sszzz");

                            timeSlot.startTime = tempStartTime;

                            slots.Add(timeSlot);
                            meeting.TimeSlot = slots.ToArray();
                        }

                        if (field.Value.Contains(_sharePointDescription))
                        {
                            meeting.Description = field.Value.Replace(_sharePointDescription + ":", string.Empty).Trim();
                        }
                        if (field.Value.Contains(_sharePointSubject))
                        {
                            meeting.subject = field.Value.Replace(_sharePointSubject + ":", string.Empty).Trim();
                        }
                        if (field.Value.Contains(_sharePointCreatedBy))
                        {
                            RequestMeetingRoomsRoomMeetingCreatedBy roomMeetingCreatedBy =
                                meeting.CreatedBy.SingleOrDefault();
                            if (roomMeetingCreatedBy != null)
                                roomMeetingCreatedBy.name =
                                    field.Value.Replace(_sharePointCreatedBy + ":", string.Empty).Trim();
                        }
                    }
                    meeting.MeetingRoomName = meetingRoomName;
                    meetings.Add(meeting);
                }
            }
            return meetings;
        }


        private RequestMeetingRoomsRoomMeeting GetCurrentMeeting(List<RequestMeetingRoomsRoomMeeting> meetings,
                                                                 string meetingRoomName)
        {
            RequestMeetingRoomsRoomMeeting currentMeeting = (from meeting in meetings
                                                             let requestMeetingRoomsRoomMeetingTimeSlot =
                                                                 meeting.TimeSlot.SingleOrDefault()
                                                             where
                                                                 requestMeetingRoomsRoomMeetingTimeSlot != null &&
                                                                 (DateTime.Parse(
                                                                     requestMeetingRoomsRoomMeetingTimeSlot.startTime) <=
                                                                  DateTime.Now
                                                                  &&
                                                                  DateTime.Parse(
                                                                      requestMeetingRoomsRoomMeetingTimeSlot.endTime) >=
                                                                  DateTime.Now
                                                                  && meeting.MeetingRoomName.Equals(meetingRoomName))
                                                             select meeting).SingleOrDefault();

            return currentMeeting;
        }

        private RequestMeetingRoomsRoomMeeting GetNextMeeting(List<RequestMeetingRoomsRoomMeeting> meetings,
                                                              string meetingRoomName)
        {
            RequestMeetingRoomsRoomMeeting NextMeeting = (from meeting in meetings
                                                          where
                                                              DateTime.Parse(
                                                                  meeting.TimeSlot.SingleOrDefault().startTime) >
                                                              DateTime.Now
                                                              && meeting.MeetingRoomName.Equals(meetingRoomName)
                                                          orderby
                                                              DateTime.Parse(
                                                                  meeting.TimeSlot.SingleOrDefault().startTime)
                                                          select meeting).FirstOrDefault();

            return NextMeeting;
        }

        private RequestMeetingRoomsRoomMeeting GetMeetingToSend(List<RequestMeetingRoomsRoomMeeting> meetings,
                                                                string meetingRoomName)
        {
            RequestMeetingRoomsRoomMeeting meeting = GetCurrentMeeting(meetings, meetingRoomName);
            if (meeting == null)
            {
                meeting = GetNextMeeting(meetings, meetingRoomName);
            }
            return meeting;
        }
    }
}