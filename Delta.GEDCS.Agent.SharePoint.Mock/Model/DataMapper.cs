﻿using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Mock.Interfaces;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Model
{
    public class DataMapper : IDataMapper
    {
        #region IDataMapper Members

        public List<MappingFields> MappingFields { get; set; }

        #endregion
    }
}