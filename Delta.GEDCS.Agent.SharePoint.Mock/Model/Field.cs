﻿using Delta.GEDCS.Agent.SharePoint.Mock.Interfaces;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Model
{
    public class Field : IField
    {
        #region IField Members

        public string GEDCSName { get; set; }
        public string SharePointName { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }

        #endregion
    }
}