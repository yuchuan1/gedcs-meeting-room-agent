﻿using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Mock.Interfaces;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Model
{
    public class MappingFields : IMappingFields
    {
        #region IMappingFields Members

        public string Type { get; set; }
        public List<Field> Fields { get; set; }

        #endregion
    }
}