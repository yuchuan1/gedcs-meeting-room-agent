using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Mock.Model;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Interfaces
{
    public interface IDataMapper
    {
        List<MappingFields> MappingFields { get; set; }
    }
}