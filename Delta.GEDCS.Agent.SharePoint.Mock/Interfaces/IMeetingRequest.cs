using System;
using System.Collections.Generic;
using Argotic.Syndication;
using Delta.GEDCS.Agent.SharePoint.Mock.Model;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Interfaces
{
    public interface IMeetingRequest
    {
        List<Field> CustomFields { get; set; }
        DataSource dataSource { get; set; }
        List<RssFeed> Feeds { get; set; }
        string MeetingInfoXML { get; set; }
        string GetMeetingInfoXML();
        string GetMeetingInfoXML(string meetingID);
        IEnumerable<RequestMeetingRoomsRoom> GetMeetingRooms();
        IEnumerable<RequestMeetingRoomsRoomMeeting> GetMeetings();
        IEnumerable<String> MeetingXMLCollection();
        Field GetSharePointField(MeetingRequest.GEDCSDefaultField gedcsField);
    }
}