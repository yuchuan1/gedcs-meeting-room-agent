using System.Collections.Generic;
using Delta.GEDCS.Agent.SharePoint.Mock.Model;

namespace Delta.GEDCS.Agent.SharePoint.Mock.Interfaces
{
    public interface IMappingFields
    {
        string Type { get; set; }
        List<Field> Fields { get; set; }
    }
}