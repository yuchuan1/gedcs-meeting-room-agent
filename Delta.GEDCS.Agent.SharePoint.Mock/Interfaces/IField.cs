namespace Delta.GEDCS.Agent.SharePoint.Mock.Interfaces
{
    public interface IField
    {
        string GEDCSName { get; set; }
        string SharePointName { get; set; }
        string Type { get; set; }
        string Size { get; set; }
    }
}