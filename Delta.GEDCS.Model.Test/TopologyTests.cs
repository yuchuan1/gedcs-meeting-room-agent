﻿// -----------------------------------------------------------------------
// <copyright file="TopologyTests.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Model.Device;
using NUnit.Framework;

namespace Delta.GEDCS.Model.Test
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [TestFixture]
    public class TopologyTests
    {
        private string AdminViewXML;

        [TestFixtureSetUp]
        public void Setup()
        {
            AdminViewXML =
                @"<?xml version=""1.0"" encoding=""utf-8""?>
            <device_topology bs_count=""3"" gedcs_version=""1.1.1"">
                <bs name=""bs1"" >                
                    <bs_ip>192.168.0.1</bs_ip>
                    <bs_mac>fe80::227:10ff:fe55:fbdc</bs_mac>
                    <bs_channel>10</bs_channel>   
                    <etags count=""2"">                    
                        <etag name=""1101"">
                            <panel_size>S7</panel_size>
                            <etag_id>0x000001</etag_id>
                        </etag>
                        <etag name=""1102"">
                            <panel_size>S7</panel_size>
                            <etag_id>0x000002</etag_id>
                        </etag>
                    </etags> 
                </bs>
                <bs name=""bs2"">
                    <bs_ip>192.168.0.2</bs_ip>
                    <bs_mac>fe80::227:10ff:xxxx:fbdc</bs_mac>
                    <bs_channel>11</bs_channel>
                    <etags count=""2"">
                        <etag name=""1201"">
                            <panel_size>S4</panel_size>
                            <etag_id>0x000003</etag_id>
                        </etag>
                        <etag name=""1202"">
                            <panel_size>S4</panel_size>
                            <etag_id>0x000004</etag_id>
                        </etag>
                    </etags>
                </bs>
                <bs name=""bs3"">
                    <bs_ip>192.168.0.3</bs_ip>
                    <bs_mac>fe80::227:10ff:xxxx:fbxx</bs_mac>
                    <bs_channel>12</bs_channel>
                    <etags count=""3"">
                        <etag name=""1301"">
                            <panel_size>S4</panel_size>
                            <etag_id>0x000005</etag_id>
                        </etag>
                        <etag name=""1302"">
                            <panel_size>S4</panel_size>
                            <etag_id>0x000000</etag_id>
                        </etag>
                        <etag name=""1303"">
                            <panel_size>S7</panel_size>
                            <etag_id>0x000000</etag_id>
                        </etag>
                    </etags>
                </bs>
            </device_topology>
            ";
        }


        [Test]
        public void GetCorrectBaseStationID()
        {
            const string meetingRoomName = "1101";
            string expectedBaseStationID = "bs1";
            string actualBaseStationID;

            var topology = new GEDCSTopology(AdminViewXML);
            actualBaseStationID = topology.GetBaseStation(meetingRoomName).BSID;

            Assert.AreEqual(expectedBaseStationID, actualBaseStationID);
        }
    }
}