﻿using System.Linq;
using Delta.GEDCS.Model.Meeting;
using NUnit.Framework;

namespace Delta.GEDCS.Model.Test
{
    [TestFixture]
    public class MeetingInfoXMLTests
    {
        private string MeetingInfo(string version)
        {
            string xml = null;
            switch (version)
            {
                case "1.5":

                    #region version 1.5

                    xml =
                        @"<?xml version=""1.0"" encoding=""utf-8""?>
                        <Request>
                            <Type type=""MeetingRoomReservation"" ver=""1.5"" />
                            <MeetingRooms count=""2"">
                                <!-- SharePoint: name=RSS title-->
                                <Room name=""1101"" meetingCount =""2"">
                                    <!-- SharePoint: sucbject=title, id=url -->
                                    <Meeting subject=""GEDCS SYSTEM DISCUSSION"">				
                                        <ID><![CDATA[http://edcssqa/Lists/1101/DispForm.aspx?ID=1]]></ID>
                                        <RecurrenceID></RecurrenceID>				
                                        <!-- 預約人 SharePoint: Created By-->
                                        <CreatedBy name= ""Eddie"" email=""email@xxx.com"" ext= ""21818""/>
                                        <!-- 預約時間起迄  start and end both in UTC date format (YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)) -->
                                        <!-- In C#, DateTime.Now.ToString(""yyyy-MM-ddTHH:mm:sszzz"") -->
                                        <TimeSlot startTime=""2012-04-24T09:10:00+08:00"" endTime=""2012-04-24T10:10:00+08:00"" />
                                        <!-- SharePoint: Description -->
                                        <Description><![CDATA[Routine Meeting...]]></Description>
                                        <!-- SharePoint: Attendees-->
                                        <Attendees count=""3""> 
                                            <Attendee name= ""Ivan"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Albert"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Nana"" email=""email@xxx.com"" ext= ""21818""/>
                                        </Attendees>				
                                    </Meeting>
                                    <Meeting subject=""ViviTeach DISCUSSION"">				
                                        <ID><![CDATA[http://edcssqa/Lists/1101/DispForm.aspx?ID=1]]></ID>
                                        <RecurrenceID></RecurrenceID>				
                                        <!-- 預約人 SharePoint: Created By-->
                                        <CreatedBy name= ""Eddie"" email=""email@xxx.com"" ext= ""21818""/>
                                        <!-- 預約時間起迄  start and end both in UTC date format-->
                                        <TimeSlot startTime=""2012-04-24T13:30:00+08:00"" endTime=""2012-04-24T17:30:00+08:00"" />
                                        <!-- SharePoint: Description -->
                                        <Description><![CDATA[Routine Meeting...]]></Description>
                                        <!-- SharePoint: Attendees-->
                                        <Attendees count=""3""> 
                                            <Attendee name= ""Ivan"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Albert"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Nana"" email=""email@xxx.com"" ext= ""21818""/>
                                        </Attendees>				
                                    </Meeting>
                                </Room>
                                <Room name=""1301"" meetingCount =""2"">
                                    <!-- SharePoint: sucbject=title, id=url -->
                                    <Meeting subject=""GEDCS SYSTEM DISCUSSION"">				
                                        <ID><![CDATA[http://edcssqa/Lists/1101/DispForm.aspx?ID=1]]></ID>
                                        <RecurrenceID></RecurrenceID>				
                                        <!-- 預約人 SharePoint: Created By-->
                                        <CreatedBy name= ""Eddie"" email=""email@xxx.com"" ext= ""21818""/>
                                        <!-- 預約時間起迄  start and end both in UTC date format (YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)) -->
                                        <!-- In C#, DateTime.Now.ToString(""yyyy-MM-ddTHH:mm:sszzz"") -->
                                        <TimeSlot startTime=""2012-04-24T09:10:00+08:00"" endTime=""2012-04-24T10:10:00+08:00"" />
                                        <!-- SharePoint: Description -->
                                        <Description><![CDATA[Routine Meeting...]]></Description>
                                        <!-- SharePoint: Attendees-->
                                        <Attendees count=""3""> 
                                            <Attendee name= ""Ivan"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Albert"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Nana"" email=""email@xxx.com"" ext= ""21818""/>
                                        </Attendees>				
                                    </Meeting>
                                    <Meeting subject=""ViviTeach DISCUSSION"">				
                                        <ID><![CDATA[http://edcssqa/Lists/1101/DispForm.aspx?ID=1]]></ID>
                                        <RecurrenceID></RecurrenceID>				
                                        <!-- 預約人 SharePoint: Created By-->
                                        <CreatedBy name= ""Eddie"" email=""email@xxx.com"" ext= ""21818""/>
                                        <!-- 預約時間起迄  start and end both in UTC date format-->
                                        <TimeSlot startTime=""2012-04-24T13:30:00+08:00"" endTime=""2012-04-24T17:30:00+08:00"" />
                                        <!-- SharePoint: Description -->
                                        <Description><![CDATA[Routine Meeting...]]></Description>
                                        <!-- SharePoint: Attendees-->
                                        <Attendees count=""3""> 
                                            <Attendee name= ""Ivan"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Albert"" email=""email@xxx.com"" ext= ""21818""/>
                                            <Attendee name= ""Nana"" email=""email@xxx.com"" ext= ""21818""/>
                                        </Attendees>				
                                    </Meeting>
                                </Room>
                            </MeetingRooms>
                        </Request>
                        ";

                    #endregion

                    break;
            }

            return xml;
        }

        private MeetingSchedule actualSchedule;

        [TestFixtureSetUp]
        public void Setup()
        {
            actualSchedule = new MeetingSchedule(MeetingInfo("1.5"));
        }

        [TestFixtureTearDown]
        public void Reset()
        {
            actualSchedule = null;
        }

        [Test]
        public void GetMeetingRoomIsCorrect()
        {
            Assert.True(actualSchedule.GetMeetingRooms().Count().Equals(2));
        }

        [Test]
        public void GetMeetingsIsCorrect()
        {
            Assert.True(actualSchedule.GetMeetings("1101").Count().Equals(2));
        }

        [Test]
        public void ParseGEDCSMeetingInfoWorks()
        {
            Assert.AreEqual("MeetingRoomReservation", ((RequestType) actualSchedule.Request.Items[0]).type);
            Assert.AreEqual("1.5", ((RequestType) actualSchedule.Request.Items[0]).ver);
            Assert.AreEqual("2", ((RequestMeetingRooms) actualSchedule.Request.Items[1]).count);

            RequestMeetingRoomsRoom room = (from item in ((RequestMeetingRooms) actualSchedule.Request.Items[1]).Room
                                            where item.name.Equals("1101")
                                            select new RequestMeetingRoomsRoom
                                                       {
                                                           name = item.name,
                                                           meetingCount = item.meetingCount,
                                                       }).SingleOrDefault();

            Assert.AreEqual("1101", room.name);
            Assert.AreEqual("2", room.meetingCount);
            Assert.AreEqual("http://edcssqa/Lists/1101/DispForm.aspx?ID=1",
                            ((RequestMeetingRooms) (actualSchedule.Request.Items[1])).Room[0].Meeting[0].ID);
        }
    }
}