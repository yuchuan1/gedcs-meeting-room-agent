﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Security.Principal;

namespace SetServiceRestart
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            if (UserIsAdministrator())
            {
                this.label1.Text = "Service has been set to restart automatically";
                using (var process = new Process())
                {
                    ProcessStartInfo processStartInfo = process.StartInfo;
                    processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    processStartInfo.FileName = "sc";
                    processStartInfo.Arguments = string.Format(" failure {0} reset= 0 actions= restart/" +
                                                               ConfigurationManager.AppSettings[
                                                                   "ConfigFileChangeServiceRestartDelay"],
                                                               "GEDCSService");

                    processStartInfo.UseShellExecute = false;
                    processStartInfo.ErrorDialog = false;
                    processStartInfo.RedirectStandardError = true;
                    processStartInfo.RedirectStandardInput = true;
                    processStartInfo.RedirectStandardOutput = true;

                    process.StartInfo = processStartInfo;
                    bool processStarted = process.Start();
                    process.WaitForExit();
                    process.Close();
                }
                Environment.Exit(0);
            }
            else
            {
                this.label1.Text = "Please run this program with adminitrator privileges.";
                Environment.Exit(0);
                
            }
        }

        public bool UserIsAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin = false;
            try
            {
                //get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                isAdmin = false;
                Environment.Exit(0);
            }
            return isAdmin;
        }
    }
}
