﻿using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Model;

namespace Delta.GEDCS.Client.Mocks
{
    public sealed class APIClient
    {
        //private static volatile APIClient instance;
        //private static object syncRoot = new Object();


        //private APIClient() { }

        //public static APIClient Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            lock (syncRoot)
        //            {
        //                if (instance == null)
        //                    instance = new APIClient();
        //            }
        //        }

        //        return instance;
        //    }
        //}

        public string GetDeviceSize(string eTagId)
        {
            if (eTagId == "notFound")
                throw new InvalidEtagNameException("eTag name: \"" + eTagId + "\" not found!");

            if (eTagId == "S4")
                return "S4";

            if (eTagId == "S7")
                return "S7";

            if (eTagId == "InvalidEtagName")
                throw new InvalidEtagNameException("eTag name: \"" + eTagId + "\" not found!");
            throw new InvalidEtagNameException("eTag name: \"" + eTagId + "\" not found!");
        }

        public string GetAdminViewXml()
        {
            return
                @"<?xml version=""1.0"" encoding=""utf-8""?>
                <device_topology bs_count=""3"" gedcs_version=""1.1.1"">
                    <bs name=""bs1"">
                        <bs_ip>192.168.0.1</bs_ip>
                        <bs_mac>fe80::227:10ff:fe55:fbdc</bs_mac>
                        <bs_channel>10</bs_channel>
                        <etags count=""2"">
                            <etag name=""1101"">
                                <panel_size>S7</panel_size>
                                <etag_id>0x000001</etag_id>
                            </etag>
                            <etag name=""1102"">
                                <panel_size>S7</panel_size>
                                <etag_id>0x000002</etag_id>
                            </etag>
                        </etags>
                    </bs>
                    <bs name=""bs2"">
                        <bs_ip>192.168.0.2</bs_ip>
                        <bs_mac>fe80::227:10ff:xxxx:fbdc</bs_mac>
                        <bs_channel>11</bs_channel>
                        <etags count=""2"">
                            <etag name=""1201"">
                                <panel_size>S4</panel_size>
                                <etag_id>0x000003</etag_id>
                            </etag>
                            <etag name=""1202"">
                                <panel_size>S4</panel_size>
                                <etag_id>0x000004</etag_id>
                            </etag>
                        </etags>
                    </bs>
                    <bs name=""bs3"">
                        <bs_ip>192.168.0.3</bs_ip>
                        <bs_mac>fe80::227:10ff:xxxx:fbxx</bs_mac>
                        <bs_channel>12</bs_channel>
                        <etags count=""3"">
                            <etag name=""1301"">
                                <panel_size>S3</panel_size>
                                <etag_id>0x000005</etag_id>
                            </etag>
                            <etag name=""1302"">
                                <panel_size>S2</panel_size>
                                <etag_id>0x000000</etag_id>
                            </etag>
                            <etag name=""1303"">
                                <panel_size>S7</panel_size>
                                <etag_id>0x000000</etag_id>
                            </etag>
                        </etags>
                    </bs>
                </device_topology>";
        }

        public GEDCSApiResponse GetJobResult(string jobId)
        {
            var response = new GEDCSApiResponse();
            switch (jobId)
            {
                case "Successful":
                    response.JobID = "Successful";
                    response.JobURL = "/api/topology/create";
                    response.JobStatus = "Successful";
                    response.MessageCode = "20000";
                    response.MessageDescription = "OK";
                    break;

                case "Working":
                    response.JobID = "Working";
                    response.JobURL = "/api/rawimage";
                    response.JobStatus = "Working";
                    response.MessageCode = "20001";
                    response.MessageDescription = "imageTitle";
                    break;

                case "Waiting":
                    response.JobID = "Waiting";
                    response.JobURL = "/api/xmlStyle";
                    response.JobStatus = "Waiting";
                    response.MessageCode = "20002";
                    response.MessageDescription = "imageTitle";
                    break;

                case "Failed":
                    response.JobID = "Waiting";
                    response.JobURL = "/api/sync/bs";
                    response.JobStatus = "Failed";
                    response.MessageCode = "40002";
                    response.MessageDescription = "Invalid BS ID";
                    break;

                case "Error":
                    response.MessageCode = "40000";
                    response.MessageDescription = "Invalid JobID";
                    break;
            }

            return response;
        }

        public GEDCSApiResponse SendPNGToEtag(string etagName, string pngFilePath)
        {
            var response = new GEDCSApiResponse();
            switch (etagName)
            {
                case "Successful":
                    response.MessageCode = "20000";
                    response.MessageDescription = "Uploaded";
                    break;

                case "InvalidEtagName":
                    response.MessageCode = "40010";
                    response.MessageDescription = "Invalid E-Tag Name";
                    throw new InvalidEtagNameException("Invalid etag name: \"" + etagName + "\"");

                case "InvalidPngFile":
                    response.MessageCode = "40006";
                    response.MessageDescription = "Invalid Image File";
                    throw new InvalidPNGException("Invalid PNG File: \"" + pngFilePath + "\"");
            }

            return response;
        }

        public GEDCSApiResponse SendPNGToEtag(string etagName, string pngFilePath, string title)
        {
            var response = new GEDCSApiResponse();
            switch (etagName)
            {
                case "Successful":
                    response.MessageCode = "20000";
                    response.MessageDescription = "Uploaded";
                    break;

                case "InvalidEtagName":
                    response.MessageCode = "40010";
                    response.MessageDescription = "Invalid E-Tag Name";
                    throw new InvalidEtagNameException("Invalid etag name: \"" + etagName + "\"");

                case "InvalidPngFile":
                    response.MessageCode = "40006";
                    response.MessageDescription = "Invalid Image File";
                    throw new InvalidPNGException("Invalid PNG File: \"" + pngFilePath + "\"");
            }

            return response;
        }

        public GEDCSApiResponse SendXMLandXSLTToEtag(string etagName, string xmlFileLocation, string xsltFileLocation)
        {
            var response = new GEDCSApiResponse();
            switch (etagName)
            {
                case "1101":
                    response.MessageCode = "20000";
                    response.MessageDescription = "Uploaded";
                    break;

                case "1202":
                    response.MessageCode = "20000";
                    response.MessageDescription = "Uploaded";
                    break;

                case "Successful":
                    response.MessageCode = "20000";
                    response.MessageDescription = "Uploaded";
                    break;

                case "InvalidEtagName":
                    response.MessageCode = "40010";
                    response.MessageDescription = "Invalid E-Tag Name";
                    throw new InvalidEtagNameException("Invalid etag name: \"" + etagName + "\"");

                case "InvalidXMLFile":
                    response.MessageCode = "40004";
                    response.MessageDescription = "Invalid XML File";
                    throw new InvalidXMLFileException("Invalid XML File: \"" + xmlFileLocation + "\"");

                case "InvalidXSLTFile":
                    response.MessageCode = "40005";
                    response.MessageDescription = "Invalid XSLT File";
                    throw new InvalidXSLTFileException("Invalid XSLT File: \"" + xsltFileLocation + "\"");
            }

            return response;
        }
    }
}