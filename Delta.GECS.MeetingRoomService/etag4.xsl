﻿<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" version="4.01" encoding="ISO-8859-1" doctype-public="-//W3C//DTD HTML 4.01//EN" />

  <xsl:template match="/Request/MeetingRooms/Room">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <title>Meeting Information</title>
        <style>
          html, body, div, span, applet, object, iframe,
          h1, h2, h3, h4, h5, h6, p, blockquote, pre,
          a, abbr, acronym, address, big, cite, code,
          del, dfn, em, img, ins, kbd, q, s, samp,
          small, strike, strong, sub, sup, tt, var,
          b, u, i, center,
          dl, dt, dd, ol, ul, li,
          fieldset, form, label, legend,
          table, caption, tbody, tfoot, thead, tr, th, td,
          article, aside, canvas, details, embed, 
          figure, figcaption, footer, header, hgroup, 
          menu, nav, output, ruby, section, summary,
          time, mark, audio, video {
          margin: 0;
          padding: 0;
          border: 0;
          vertical-align: baseline;
          }
          ol, ul {
          list-style: none;
          }
		
          div#current {
          width: 300px;
          height: 220px;
          padding: 10px;
          }
		
          body, div, p, th, td {
          font-family: "Arial Unicode MS", "微軟正黑體", "Arial";
          font-size: 11pt;
          }
		
          p.header {
          font-size: 16pt;
          }
		
          li {
          border-top: 1px solid gray;
          width: 300px;
          vertical-align: middle;
          width: 300px;
          max-width: 300px;
          text-overflow: ellipsis;
          overflow: hidden;
          pad
          }
		
          label {
          color: #ffffff;
          background-color: #000000;
          display: inline-block;
          text-align: right;
          width: 50px;
          padding: 3px;
          }
          label.noshow {
          width: 0px;
          }
		
          span {
          padding: 3px;
          }
          span.contact {
          display: inline-block;
          width: 130px;
          }
          span.small {
          font-size: 9pt;
          }
          #ext {
          width: 35px;
          text-align: center;
          }
        </style>
      </head>
      <body topmargin="0" leftmargin="0">

        <div id="current">
          <p class="header">
            會議室
            <xsl:value-of select="@name" />
          </p>
          <ol>
            <li>
              <label>主題</label>
              <span>
                <xsl:value-of select="Meeting/@subject" />
              </span>
            </li>
            <li>
              <label>時間</label>
              <span>
                <xsl:value-of select="Meeting/TimeSlot/@startTime" />
                ~
                <xsl:value-of select="Meeting/TimeSlot/@endTime" />
              </span>
            </li>
            <li>
              <label>主席</label>
              <span>
                <xsl:value-of select="Meeting/RecurrenceID" />
              </span>
            </li>
            <li>
              <label>預約者</label>
              <span class="contact">
                <xsl:value-of select="Meeting/CreatedBy/@name" />
              </span>
              <label id="ext">分機</label>
              <span>
                <xsl:value-of select="Meeting/CreatedBy/@ext" />
              </span>
            </li>
            <li>
              <label>E-mail</label>
              <span>
                <xsl:value-of select="Meeting/CreatedBy/@email" />
              </span>
            </li>
            <li>
              <span>
                <xsl:value-of select="Meeting/Description" />
              </span>
            </li>
            <li>
              <span class="small">
                <xsl:value-of select="Meeting/ID" />
              </span>
            </li>
          </ol>
        </div>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>