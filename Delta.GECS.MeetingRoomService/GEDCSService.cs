﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using Delta.GEDCS.Jobs;
using NLog;

namespace Delta.GEDCS.MeetingRoomService
{
    public partial class GEDCSService : ServiceBase
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string CurrentDir =
            Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);

        private readonly GEDCSScheduler scheduler;
        private readonly JobMonitor jobMonitor = JobMonitor.Instance;
        private CancellationTokenSource cancellationToken;
        private List<FileSystemWatcher> watchers;

        public GEDCSService()
        {
            InitializeComponent();
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            _logger.Debug("Starting GEDCSScheduler...");
            scheduler =
                new GEDCSScheduler(GEDCSScheduler.SchedulerType.SharePoint);
        }

        protected override void OnStart(string[] args)
        {
            cancellationToken = new CancellationTokenSource();
            var taskJobMonitor = new Task(() => jobMonitor.MonitorJobs(), cancellationToken.Token);
            taskJobMonitor.Start();

            scheduler.Start();
            _logger.Debug("GEDCSScheduler started");

            _logger.Debug("Starting file system watcher...");
            _logger.Debug("Monitoring config files under " + CurrentDir);
            try
            {
                string[] filters = {"*.xml", "*.config", "*.xsl"};
                watchers = new List<FileSystemWatcher>();
                foreach (string f in filters)
                {
                    var w = new FileSystemWatcher();
                    w.Filter = f;
                    w.Path = CurrentDir;
                    w.Changed += new FileSystemEventHandler(LogFileSystemChanged);
                    w.EnableRaisingEvents = true;
                    watchers.Add(w);
                }
            }
            catch (Exception ex)
            {
                scheduler.Shutdown();
                _logger.Fatal("File monitor failed: " + ex.Message);
                Stop();
            }

            _logger.Debug("Config file monitoring started.");
        }

        private void LogFileSystemChanged(object sender, FileSystemEventArgs e)
        {
            _logger.Debug("File change detected. Restarting service... ( Detected by " + ((FileSystemWatcher)sender).Filter + ")");
            //Remember to set the service to restart after failure 
            Environment.Exit(1);
        }

        protected override void OnStop()
        {
            scheduler.Stop();
            scheduler.Shutdown();
            foreach (FileSystemWatcher watcher in watchers)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Dispose();
            }
            cancellationToken.Cancel();
            _logger.Debug("Config file monitoring Stopped.");
        }

        protected override void OnPause()
        {
            scheduler.Pause();
        }

        protected override void OnContinue()
        {
            scheduler.Resume();
        }

        protected override void OnShutdown()
        {
            scheduler.Shutdown();
            foreach (FileSystemWatcher watcher in watchers)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Dispose();
            }
            cancellationToken.Cancel();
        }
    }
}