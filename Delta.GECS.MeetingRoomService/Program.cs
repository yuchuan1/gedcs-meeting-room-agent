﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using NLog;

namespace Delta.GEDCS.MeetingRoomService
{
    internal static class Program
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
                                {
                                    new GEDCSService()
                                };

            //foreach (ServiceBase serviceBase in ServicesToRun)
            //{
            //    SetRecoveryOptions(serviceBase.ServiceName);
            //}
            ServiceBase.Run(ServicesToRun);
        }

        //private static void SetRecoveryOptions(string serviceName)
        //{
        //    int exitCode;
        //    using (var process = new Process())
        //    {
        //        ProcessStartInfo startInfo = process.StartInfo;
        //        startInfo.FileName = "sc";
        //        startInfo.WindowStyle = ProcessWindowStyle.Hidden;

        //        startInfo.UseShellExecute = false;
        //        startInfo.ErrorDialog = false;

        //        startInfo.RedirectStandardError = true;
        //        startInfo.RedirectStandardInput = true;
        //        startInfo.RedirectStandardOutput = true;

        //        // tell Windows that the service should restart if it fails
        //        _logger.Debug("Setting the service to restart itself after config file change");

        //        try
        //        {
        //            int RestartDelay = int.Parse(ConfigurationManager.AppSettings["ConfigFileChangeServiceRestartDelay"]);

        //            startInfo.Arguments = string.Format("failure {0} reset= 0 actions= restart/" +
        //                                                RestartDelay.ToString(), serviceName);

        //            process.Start();
        //            StreamWriter inputWriter = process.StandardInput;
        //            StreamReader outputReader = process.StandardOutput;
        //            StreamReader errorReader = process.StandardError;
        //            process.WaitForExit();

        //            exitCode = process.ExitCode;

        //            process.Close();

        //            _logger.Error(errorReader.ReadToEnd());
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Debug(ex.Message);
        //            throw;
        //        }
        //    }

        //    if (exitCode != 0)
        //        throw new InvalidOperationException();
        //}
    }
}