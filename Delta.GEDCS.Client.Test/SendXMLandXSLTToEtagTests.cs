﻿// -----------------------------------------------------------------------
// <copyright file="SendXMLandXSLTToEtag.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Model;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Test
{
    [TestFixture]
    public class SendXMLandXSLTToEtagTests
    {
        //APIClient client = new APIClient();

        private readonly APIClient _client = new APIClient();

        [Test]
        public void SendXMLandXSLTToEtagSuccess()
        {
            string EtagName = "1";
            string xmlFileLocation = "";
            string xsltFileLocation = "";

            var expectedResponse = new GEDCSApiResponse();
            expectedResponse.JobID = "1";
            expectedResponse.MessageCode = "20000";
            expectedResponse.MessageDescription = "Uploaded";

            GEDCSApiResponse actualResponse = _client.SendPNGToEtag(EtagName, xmlFileLocation, xsltFileLocation);

            Assert.AreEqual(actualResponse.JobID, expectedResponse.JobID);
            Assert.AreEqual(actualResponse.MessageCode, expectedResponse.MessageCode);
            Assert.AreEqual(actualResponse.MessageDescription, expectedResponse.MessageDescription);
        }

//        [Test]
//        [ExpectedException(typeof(InvalidEtagNameException))]
//        public void SendXMLandXSLTToInvalidEtagFailedAsExpected()
//        {
//            string EtagName = "wrontEtag";
//            string xmlFileLocation = "";
//            string xsltFileLocation = "";
//            client.SendXMLandXSLTToEtag
//            Assert.AreEqual(client.SendXMLandXSLTToEtag(EtagName, xmlFileLocation, xsltFileLocation), @"{
//                ""messageCode"":""40010"",
//                ""messageDescription"":""Invalid E-Tag Name""
//                }");
//        }
    }
}