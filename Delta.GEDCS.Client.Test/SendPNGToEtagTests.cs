﻿// -----------------------------------------------------------------------
// <copyright file="SendPNGToEtagTests.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Model;
using NUnit.Framework;

namespace Delta.GEDCS.Client.IntegrationTest
{
    [TestFixture]
    public class SendPngToEtagTests
    {
        //private readonly APIClient _client;
        private readonly APIClient _client = new APIClient();

        [Test]
        public void GetAdminViewIsWorking()
        {
            Assert.True(_client.GetAdminViewXml().Contains("1301"));
        }

        [Test]
        [ExpectedException(typeof (InvalidPNGException))]
        public void SendInvalidPngToEtagFailedAsExpected()
        {
            const string etagName = "a";
            const string pngFilePath = "InvalidPNG";

            _client.SendPNGToEtag(etagName, pngFilePath);
        }

        [Test]
        public void SendPngToEtagSuccess()
        {
            string EtagName = "1";
            string xmlFileLocation = "";
            string xsltFileLocation = "";

            var expectedResponse = new GEDCSApiResponse();
            expectedResponse.JobID = "1";
            expectedResponse.MessageCode = "20000";
            expectedResponse.MessageDescription = "Uploaded";

            GEDCSApiResponse response = _client.SendPNGToEtag(EtagName, xmlFileLocation, xsltFileLocation);

            Assert.AreEqual(response.MessageCode, expectedResponse.MessageCode);
            Assert.AreEqual(response.JobID, expectedResponse.JobID);
            Assert.AreEqual(response.MessageDescription, expectedResponse.MessageDescription);
        }

        [Test]
        [ExpectedException(typeof (InvalidEtagNameException))]
        public void SendPngToInvalidEtagFailedAsExpected()
        {
            string EtagName = "wrongEtag";
            string PNGFilePath = "";
            GEDCSApiResponse response = _client.SendPNGToEtag(EtagName, PNGFilePath);
        }
    }
}