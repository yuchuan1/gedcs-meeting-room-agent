﻿// -----------------------------------------------------------------------
// <copyright file="GetDeviceSizeTests.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Exceptions;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Test
{
    [TestFixture]
    public class GetDeviceSizeTests
    {
        private readonly APIClient _client = new APIClient();

        [Test]
        [ExpectedException(typeof (InvalidEtagNameException))]
        public void GetDeviceSizeFailedAsExpected()
        {
            const string etagId = "notFound";
            _client.GetDeviceSize(etagId);
        }

        [Test]
        public void GetDeviceSizeSuccess()
        {
            const string etagId = "etag4";
            const string expectedReturnValue = @"S7";

            Assert.AreEqual(_client.GetDeviceSize(etagId), expectedReturnValue);
        }
    }
}