﻿// -----------------------------------------------------------------------
// <copyright file="GetJobResultTests.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Interfaces;
using Delta.GEDCS.Client.Model;
using Moq;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Test
{
    [TestFixture]
    public class GetJobResultTests
    {
        private APIClient _client = new APIClient();

        public void GetJobResultSuccess()
        {
            const string JobID = "1";
            var expectedReturnValue = new GEDCSApiResponse
                                          {
                                              JobID = "1",
                                              JobURL = "/api/topology/create",
                                              JobStatus = "Successful",
                                              MessageCode = "20000",
                                              MessageDescription = "OK"
                                          };


            var mock = new Mock<IAPIClient>();
            mock.Setup(getJobResult => getJobResult.GetJobResult(JobID))
                .Returns(expectedReturnValue);

            Assert.AreEqual(mock.Object.GetJobResult(JobID), expectedReturnValue);
        }

        [Test]
        [ExpectedException(typeof (InvalidJobIDException))]
        public void GetJobResultInvalidJobIdFailedAsExpected()
        {
            const string jobId = "2";

            var mock = new Mock<IAPIClient>();
            mock.Setup(getJobResult => getJobResult.GetJobResult(jobId))
                .Throws(new InvalidJobIDException());

            mock.Object.GetJobResult(jobId);
        }
    }
}