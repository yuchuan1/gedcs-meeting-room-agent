﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Delta.GEDCS;
using Delta.GEDCS.Client.APIs;
using Delta.GEDCS.Client.Model;
using Delta.GEDCS.Jobs;
using NLog;
using Quartz;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly JobMonitor jobMonitor = JobMonitor.Instance;
        private readonly GEDCSScheduler scheduler;
        private Task taskJobMonitor;
        private CancellationTokenSource cancellationToken;

        public Form1()
        {
            cancellationToken = new CancellationTokenSource();
            taskJobMonitor = new Task(() =>
                                          {
                                              cancellationToken.Token.ThrowIfCancellationRequested();
                                              jobMonitor.MonitorJobs();
                                          }, cancellationToken.Token);

            taskJobMonitor.Start();
            InitializeComponent();

            _logger.Debug("Initializing GEDCSScheduler");
            scheduler =
                new GEDCSScheduler(GEDCSScheduler.SchedulerType.SharePoint);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            scheduler.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var test = new Test();
            test.TestRun(jobMonitor);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            scheduler.Stop();
            cancellationToken.Cancel();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            APIClient api = new APIClient();
            GEDCSApiResponse response;
            if(checkBox1.Checked)
                response = api.SendPNGToEtag( txtEtagName.Text, "doraemon.png");
            else
                response = api.SendPNGToEtag(txtEtagName.Text, "raw.png");

            JobMonitor jobMonitor = JobMonitor.Instance;
            jobMonitor.AddJob(response);
            int i;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            APIClient api = new APIClient();
            JobMonitor jobMonitor = JobMonitor.Instance;
            GEDCSApiResponse response = new GEDCSApiResponse();

            //for (int i = 0; i < 10; i++)
            //{
            //    response = api.SendPNGToEtag("target01", "raw.png");
            //    jobMonitor.AddJob(response);
            //}

            Parallel.For(0, 100, (i) =>
            {
                response = api.SendPNGToEtag("target01", "raw.png");
                response = api.SendPNGToEtag("target01", "doraemon.png");
              //  jobMonitor.AddJob(response);
            });

            //List<byte[]> pngFiles = new List<byte[]>();
            //for (int i = 0; i < 10; i++)
            //{
            //    pngFiles.Add(BMP_to_PNG(Convert_Text_to_Image(i.ToString(), "Arial", 20)));
            //}

            //Parallel.ForEach(pngFiles, file =>
            //                               {
            //                                   response = api.SendPNGToEtag("target01", file);
            //                                   jobMonitor.AddJob(response);
            //                               });

            //foreach (byte[] file in pngFiles)
            //{
            //    response = api.SendPNGToEtag("target01", file);
            //    jobMonitor.AddJob(response);
            //}

        }

        public static Bitmap Convert_Text_to_Image(string txt, string fontname, int fontsize)
        {
            //creating bitmap image
            Bitmap bmp = new Bitmap(1, 1);

            //FromImage method creates a new Graphics from the specified Image.
            Graphics graphics = Graphics.FromImage(bmp);
            // Create the Font object for the image text drawing.
            Font font = new Font(fontname, fontsize);
            // Instantiating object of Bitmap image again with the correct size for the text and font.
            SizeF stringSize = graphics.MeasureString(txt, font);
            bmp = new Bitmap(bmp, (int)stringSize.Width, (int)stringSize.Height);
            graphics = Graphics.FromImage(bmp);

            /* It can also be a way
           bmp = new Bitmap(bmp, new Size((int)graphics.MeasureString(txt, font).Width, (int)graphics.MeasureString(txt, font).Height));*/

            //Draw Specified text with specified format 
            graphics.DrawString(txt, font, Brushes.Red, 0, 0);
            font.Dispose();
            graphics.Flush();
            graphics.Dispose();
            return bmp;     //return Bitmap Image 
        }

        public static byte[] BMP_to_PNG(Bitmap bmp)
        {
            byte[] result = null;
            using (MemoryStream stream = new MemoryStream())
            {
                bmp.Save(stream, ImageFormat.Png);
                result = stream.ToArray();
            }

            return result;
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
            for (int i = 0; i < 100; i++)
            {
                var test = new Test();
                test.TestRun(jobMonitor);
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
           // string conn = "Data Source=.\\ScheduleDB.sdf";
           // ScheduleDB db = new ScheduleDB(conn);

           // Meeting m = new Meeting();
           // m.JobId = "This is a test";
           // m.MeetingRoomName = "This is a test";
           // m.Sent = false;
           // m.Xml = "This is a test";

           //db.Meeting.InsertOnSubmit(m);
           //db.SubmitChanges();

            //SqlCeConnection conn = new
            //    SqlCeConnection("Data Source=ScheduleDB.sdf;Persist Security Info=False;");
            //conn.Open();

            //SqlCeCommand cmd = new SqlCeCommand(
            //    "insert into meeting ([JobId], [MeetingRoomName], [Sent], [xml] ) VALUES('This is a test', 'test meeting room', 0, 'Test xml')", conn);

            //cmd.ExecuteNonQuery();

            string conns = "Data Source=ScheduleDB.sdf;Persist Security Info=False;";
            ScheduleDB db = new ScheduleDB(conns);

            Meeting m = new Meeting();
            m.JobId = "This is a test";
            m.MeetingRoomName = "This is a test";
            m.Sent = false;
            m.Xml = "This is a test";

            db.Meeting.InsertOnSubmit(m);
            db.SubmitChanges();

            var temp = from i in db.Meeting
                       select i;

            foreach (var meeting in temp )
            {
                button6.Text = meeting.JobId;
            }



        }
    }
}