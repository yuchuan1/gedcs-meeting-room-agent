<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" version="4.01" encoding="ISO-8859-1" doctype-public="-//W3C//DTD HTML 4.01//EN"/>

<xsl:template match="/Request/MeetingRooms/Room">
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=8"/>
	<title>Meeting Information</title>
	<style>
	body, div, p, th, td {
		font-family: Arial;
	}
	</style>
  </head>
  <body topmargin="0" leftmargin="0">

<table width="320" height="480" border="0" cellspacing="0" cellpadding="20">
<tr><td>

	<div id="current">
		<h1>會議室 <xsl:value-of select="@name"/></h1>
		<hr></hr>
		<ol>
			<li>
				<label>會議議題</label>
				<span><xsl:value-of select="Meeting/@subject"/></span>
			</li>
			<li>
				<label>時段</label>
				<span>
					<xsl:value-of select="Meeting/TimeSlot/@startTime"/> ~ 
					<xsl:value-of select="Meeting/TimeSlot/@endTime"/>
				</span>
			</li>
			<li><label>主席</label><span><xsl:value-of select="Meeting/RecurrenceID"/></span></li>
			<li><label>預約者</label><span><xsl:value-of select="Meeting/CreatedBy/@name"/></span></li>
			<li><label></label><span><xsl:value-of select="Meeting/CreatedBy/@email"/></span></li>
			<li><label></label><span><xsl:value-of select="Meeting/CreatedBy/@ext"/></span></li>
			<li><label></label><span><xsl:value-of select="Meeting/ID"/></span></li>
			<li><label></label><span><xsl:value-of select="Meeting/Description"/></span></li>
		</ol>
	</div>

	</td></tr>
	</table>

	</body>
  </html>
</xsl:template>

</xsl:stylesheet>
