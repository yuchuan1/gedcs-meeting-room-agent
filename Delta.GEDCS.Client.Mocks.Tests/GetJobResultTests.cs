﻿using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Interfaces;
using Delta.GEDCS.Client.Model;
using Moq;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Mocks.Tests
{
    [TestFixture]
    public class GetJobResultTests
    {
        private readonly APIClient _client = new APIClient();

        [Test]
        [ExpectedException(typeof (InvalidJobIDException))]
        public void GetJobResultInvalidJobIdFailedAsExpected()
        {
            const string jobId = "2";

            var mock = new Mock<IAPIClient>();
            mock.Setup(getJobResult => getJobResult.GetJobResult(jobId))
                .Throws(new InvalidJobIDException());

            mock.Object.GetJobResult(jobId);
        }

        [Test]
        public void GetJobResultSuccess()
        {
            string JobID = "Successful";
            GEDCSApiResponse expectedReturnValue = _client.GetJobResult(JobID);
            var actualReturnValue = new GEDCSApiResponse
                                        {
                                            JobID = "Successful",
                                            JobURL = "/api/topology/create",
                                            JobStatus = "Successful",
                                            MessageCode = "20000",
                                            MessageDescription = "OK"
                                        };

            Assert.AreEqual(actualReturnValue.JobID, expectedReturnValue.JobID);
            Assert.AreEqual(actualReturnValue.JobURL, expectedReturnValue.JobURL);
            Assert.AreEqual(actualReturnValue.JobStatus, expectedReturnValue.JobStatus);
            Assert.AreEqual(actualReturnValue.MessageCode, expectedReturnValue.MessageCode);
            Assert.AreEqual(actualReturnValue.MessageDescription, expectedReturnValue.MessageDescription);
        }
    }
}