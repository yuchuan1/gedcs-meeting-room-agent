﻿using Delta.GEDCS.Client.Exceptions;
using Delta.GEDCS.Client.Model;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Mocks.Tests
{
    [TestFixture]
    public class SendPngToEtagTests
    {
        //private readonly APIClient _client;
        private readonly APIClient _client = new APIClient();

        [Test]
        [ExpectedException(typeof (InvalidPNGException))]
        public void SendInvalidPngToEtagFailedAsExpected()
        {
            const string etagName = "InvalidPngFile";
            const string pngFilePath = "InvalidPNG";

            _client.SendPNGToEtag(etagName, pngFilePath);
        }

        [Test]
        public void SendPngToEtagSuccess()
        {
            string etagName = "Successful";

            var expectedResponse = new GEDCSApiResponse
                                       {MessageCode = "20000", MessageDescription = "Uploaded"};

            GEDCSApiResponse actualResponse = _client.SendPNGToEtag(etagName, "", "");

            Assert.AreEqual(actualResponse.MessageCode, expectedResponse.MessageCode);
            Assert.AreEqual(actualResponse.JobID, expectedResponse.JobID);
            Assert.AreEqual(actualResponse.MessageDescription, expectedResponse.MessageDescription);
        }

        [Test]
        [ExpectedException(typeof (InvalidEtagNameException))]
        public void SendPngToInvalidEtagFailedAsExpected()
        {
            string EtagName = "InvalidEtagName";
            string PNGFilePath = "";
            GEDCSApiResponse response = _client.SendPNGToEtag(EtagName, PNGFilePath);
        }
    }
}