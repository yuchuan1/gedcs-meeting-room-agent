﻿using Delta.GEDCS.Client.Model;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Mocks.Tests
{
    [TestFixture]
    public class SendXMLandXSLTToEtagTests
    {
        private readonly APIClient _client = new APIClient();

        [Test]
        public void SendXMLandXSLTToEtagSuccess()
        {
            string etagName = "Successful";
            string xmlFileLocation = "";
            string xsltFileLocation = "";

            var expectedResponse = new GEDCSApiResponse {MessageCode = "20000", MessageDescription = "Uploaded"};

            GEDCSApiResponse actualResponse = _client.SendXMLandXSLTToEtag(etagName, xmlFileLocation, xsltFileLocation);

            Assert.AreEqual(actualResponse.MessageCode, expectedResponse.MessageCode);
            Assert.AreEqual(actualResponse.MessageDescription, expectedResponse.MessageDescription);
        }
    }
}