﻿using Delta.GEDCS.Client.Exceptions;
using NUnit.Framework;

namespace Delta.GEDCS.Client.Mocks.Tests
{
    [TestFixture]
    public class GetDeviceSizeTests
    {
        private APIClient _client;

        [TestFixtureSetUp]
        public void Setup()
        {
            _client = new APIClient();
        }

        [Test]
        [ExpectedException(typeof (InvalidEtagNameException))]
        public void GetDeviceSizeFailedAsExpected()
        {
            const string etagId = "notFound";
            _client.GetDeviceSize(etagId);
        }

        [Test]
        public void GetDeviceSizeSuccess()
        {
            const string etagId = "S7";
            const string expectedReturnValue = @"S7";

            Assert.AreEqual(_client.GetDeviceSize(etagId), expectedReturnValue);
        }
    }
}